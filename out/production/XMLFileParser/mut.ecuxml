﻿<?xml version="1.0" encoding="utf-8" ?>
<parameters namespace='Mitsu' description="Custom Mitsubishi MUT sensors">

  <disable-obd2>false</disable-obd2>

  <init-string>
    ATSP0; ATIB10;
  </init-string>

<!--
  <parameter id="MIL">
    <class>NissanMILSensor</class>
  </parameter>

  <parameter id="ClearDTC">
    <raw> 14 </raw>
  </parameter>
-->
<!--
  This ECU has only 1-byte header, therefore first byte is at -1 offset (because hobdrive treats standard header is of 2 bytes always)
  Therefore, using value - -1 for this
-->
  <parameter id="TimingAdvanceInterpolated">
    <description>
      <name>Timing Advance Interpolated</name>
      <unit>degree</unit>
    </description>
    <raw> A004 </raw>
    <value--1> 1 </value--1>
    <offset> -20 </offset>
  </parameter>

  <parameter id="TimingAdvanceScaled">
    <description>
      <name>Timing Advance Scaled</name>
      <unit>degree</unit>
    </description>
    <raw> A005 </raw>
    <value--1> 1 </value--1>
    <offset> -20 </offset>
  </parameter>

  <parameter id="TimingAdvance">
    <description>
      <name>Timing Advance</name>
      <unit>degree</unit>
    </description>
    <raw> A006 </raw>
    <value--1> 1 </value--1>
    <offset> -20 </offset>
  </parameter>

  <parameter id="CoolantTemp_raw">
    <description>
      <name>Coolant Temperature</name>
      <unit>celsius</unit>
    </description>
    <raw> A007 </raw>
    <value--1> 1 </value--1>
    <offset> 0 </offset>
  </parameter>

  <parameter id="FuelTrim_Low">
    <description>
      <name>Fuel Trim Low (LTFT)</name>
      <unit>percent</unit>
    </description>
    <raw> A00C </raw>
    <value--1> 0.1961 </value--1>
    <offset> -25 </offset>
  </parameter>

  <parameter id="FuelTrim_Mid">
    <description>
      <name>Fuel Trim Mid (LTFT)</name>
      <unit>percent</unit>
    </description>
    <raw> A00D </raw>
    <value--1> 0.1961 </value--1>
    <offset> -25 </offset>
  </parameter>

  <parameter id="FuelTrim_High">
    <description>
      <name>Fuel Trim High (LTFT)</name>
      <unit>percent</unit>
    </description>
    <raw> A00E </raw>
    <value--1> 0.1961 </value--1>
    <offset> -25 </offset>
  </parameter>

  <parameter id="O2FeedbackTrim">
    <description>
      <name>Oxygen Feedback Trim (STFT)</name>
      <unit>percent</unit>
    </description>
    <raw> A00F </raw>
    <value--1> 0.1961 </value--1>
    <offset> -25 </offset>
  </parameter>

  <parameter id="CoolantTempScaled">
    <description>
      <name>Coolant Temperature</name>
      <unit>celsius</unit>
    </description>
    <raw> A010 </raw>
    <value--1> 1 </value--1>
    <offset> -40 </offset>
  </parameter>

  <parameter id="MAFAirTempScaled">
    <description>
      <name>MAF Air Temp Scaled</name>
      <unit>celsius</unit>
    </description>
    <raw> A011 </raw>
    <value--1> 1 </value--1>
    <offset> -40 </offset>
  </parameter>

  <parameter id="O2Sensor">
    <description>
      <name>Oxygen Sensor</name>
      <unit>volts</unit>
    </description>
    <raw> A013 </raw>
    <value--1> 0.01952 </value--1>
    <offset> 0 </offset>
  </parameter>

  <parameter id="BatteryLevel">
    <description>
      <name>Battery Level</name>
      <unit>volts</unit>
    </description>
    <raw> A014 </raw>
    <value--1> 0.07333 </value--1>
    <offset> 0 </offset>
  </parameter>

  <parameter id="Barometer">
    <description>
      <unit>kPa</unit>
    </description>
    <raw> A015 </raw>
    <value--1> 0.49 </value--1>
    <offset> 0 </offset>
  </parameter>

  <parameter id="ISCSteps">
    <description>
      <name>ISC Steps</name>
    </description>
    <raw> A016 </raw>
    <value--1> 1 </value--1>
    <offset> 0 </offset>
  </parameter>

  <parameter id="TPS">
    <description>
      <name>Throttle Position</name>
      <unit>percent</unit>
    </description>
    <raw> A017 </raw>
    <value--1> 0.392 </value--1>
    <offset> 0 </offset>
  </parameter>

  <parameter id="Open_Loop_Bit_Array">
    <raw> A018 </raw>
    <value--1> 1 </value--1>
  </parameter>

  <parameter id="AirFlow">
    <description>
      <name>Air Flow Hz</name>
      <unit>Hz</unit>
    </description>
    <raw> A01A </raw>
    <value--1> 6.25 </value--1>
    <offset> 0 </offset>
  </parameter>

  <parameter id="Manifold_Absolute_Pressure">
    <description>
      <name>Manifold Absolute Pressure</name>
      <unit>kpa</unit>
    </description>
    <raw> A01A </raw>
    <value--1> 0.49 </value--1>
  </parameter>

  <parameter id="TPS_Idle_Adder">
    <raw> A01B </raw>
    <value--1> 1 </value--1>
  </parameter>

  <parameter id="ECULoad">
    <description>
      <name>ECULoad</name>
      <unit>percent</unit>
    </description>
    <raw> A01C </raw>
    <value--1> 0.625 </value--1>
    <offset> 0 </offset>
  </parameter>

  <parameter id="AccelEnrich">
    <description>
      <name>Airflow/Rev</name>
    </description>
    <raw> A01D </raw>
    <value--1> 0.7843 </value--1>
    <offset> 0 </offset>
  </parameter>

  <parameter id="Manifold_Absolute_Pressure_Mean">
    <description>
      <name>Manifold Absolute Pressure Mean</name>
      <unit>kpa</unit>
    </description>
    <raw> A01D </raw>
    <value--1> 0.49 </value--1>
  </parameter>

  <parameter id="ClosedLoop">
    <description>
      <name>Work on closed Loop</name>
      <unit>bit</unit>
    </description>
    <raw> A01E </raw>
    <value--1> 1 </value--1>
    <bit>7</bit>
  </parameter>

  <parameter id="Engine_RPM_Idle_Scaled">
    <description>
      <name>Engine RPM Idle Scaled</name>
      <unit>rpm</unit>
    </description>
    <raw> A020 </raw>
    <value--1> 31.25 </value--1>
    <offset> 0 </offset>
  </parameter>

  <parameter id="Engine_RPM">
    <description>
      <name>Engine RPM</name>
      <unit>rpm</unit>
    </description>
    <raw> A021 </raw>
    <value--1> 31.25 </value--1>
    <offset> 0 </offset>
  </parameter>

  <parameter id="TargetIdleRPM">
    <description>
      <name>Target Idle RPM</name>
      <unit>rpm</unit>
    </description>
    <raw> A024 </raw>
    <value--1> 7.8 </value--1>
    <offset> 0 </offset>
  </parameter>

  <parameter id="KnockSum">
    <description>
      <name>Knock Sum</name>
    </description>
    <raw> A026 </raw>
    <value--1> 1 </value--1>
    <offset> 0 </offset>
  </parameter>

  <parameter id="OctaneFlag">
    <description>
      <name>Octane Level</name>
      <unit>percent</unit>
    </description>
    <raw> A027 </raw>
    <value--1> 0.392 </value--1>
    <offset> 0 </offset>
  </parameter>

  <parameter id="InjectorPulseWidth">
    <description>
      <name>Injector Pulse Width</name>
      <unit>ms</unit>
    </description>
    <raw> A029 </raw>
    <value--1> 0.256 </value--1>
    <offset> 0 </offset>
  </parameter>

  <parameter id="InjectorPulseWidthAlt">
    <description>
      <name>Injector Pulse Width Alt</name>
      <unit>ms</unit>
    </description>
    <raw> A02A </raw>
    <value--1> 0.256 </value--1>
    <offset> 0 </offset>
  </parameter>

  <parameter id="AirVol">
    <description>
      <name>Air Volume</name>
    </description>
    <raw> A02C </raw>
    <value--1> 1 </value--1>
    <offset> 0 </offset>
  </parameter>

  <parameter id="Speed">
    <description>
      <name>Speed</name>
      <unit>km/h</unit>
    </description>
    <raw> A02F </raw>
    <value--1> 2 </value--1>
    <offset> 0 </offset>
  </parameter>

  <parameter id="Knock">
    <description>
      <name>Knock Voltage</name>
      <unit>volts</unit>
    </description>
    <raw> A030 </raw>
    <value--1> 0.0195 </value--1>
    <offset> 0 </offset>
  </parameter>

   <parameter id="VolumetricEfficiency">
    <raw> A031 </raw>
    <value--1> 0.0195 </value--1>
    <offset> 0 </offset>
  </parameter>

   <parameter id="AFR_Inv">
    <raw> A032 </raw>
    <value--1> 0.11484375 </value--1>
    <offset> 0 </offset>
  </parameter>

  <parameter id="TimingAdvanceCorr">
    <description>
      <name>Timing Advance Corrected</name>
      <unit>degree</unit>
    </description>
    <raw> A033 </raw>
    <value--1> 1 </value--1>
    <offset> -20 </offset>
  </parameter>

  <parameter id="MAP_Bar">
    <description>
      <name>Boost (MDP)</name>
      <unit>Bar</unit>
    </description>
    <raw> A038 </raw>
    <value--1> 0.01334 </value--1>
    <offset> 0 </offset>
  </parameter>

  <parameter id="O2Sensor2">
    <description>
      <name>Oxygen Sensor 2</name>
      <unit>volts</unit>
    </description>
    <raw> A03C </raw>
    <value--1> 0.01952 </value--1>
    <offset> 0 </offset>
  </parameter>

  <parameter id="ACSwitch">
    <description>
      <name>Air Conditioning Switch</name>
      <unit>bit</unit>
    </description>
    <raw> A0B8 </raw>
    <value--1> 1 </value--1>
    <bit>0</bit>
  </parameter>

</parameters>
