package tirex.ezway.libs.ecu;

import tirex.ezway.libs.Parameter;

import java.util.ArrayList;

public class micas_103_E3 extends DefaultAuto {
    public micas_103_E3() {
        ArrayList<Parameter> parameters = new ArrayList<Parameter>();
        parameters.add(new Parameter.Builder("ADC_Detonation", "21E2", new int[] {2, 3}, "В", "V").multiplier(0.004885).build());
        parameters.add(new Parameter.Builder("ADC_MAP", "21E2", new int[] {4, 5}, "В", "V").multiplier(0.004885).build());
        parameters.add(new Parameter.Builder("ADC_Oxygen_b1s2", "21E2", new int[] {6, 7}, "В", "V").multiplier(0.004885).build());
        parameters.add(new Parameter.Builder("ADC_Oxygen_b1s1", "21E2", new int[] {8, 9}, "В", "V").multiplier(0.004885).build());
        parameters.add(new Parameter.Builder("ADC_CoolantTemp", "21E2", new int[] {12, 13}, "В", "V").multiplier(0.004885).build());
        parameters.add(new Parameter.Builder("ADC_ThrottlePosition", "21E2", new int[] {18, 19}, "В", "V").multiplier(0.004885).build());
        micas_103_E3.super.setParameters(parameters);
    }
}
