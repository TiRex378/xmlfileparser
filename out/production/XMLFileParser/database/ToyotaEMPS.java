package tirex.ezway.libs.ecu;

import tirex.ezway.libs.Parameter;

import java.util.ArrayList;

public class ToyotaEMPS extends DefaultAuto {
    public ToyotaEMPS() {
        ArrayList<Parameter> parameters = new ArrayList<Parameter>();
        parameters.add(new Parameter.Builder("TrqSen1Out", "2103", new int[] {0}, "В", "V").multiplier(0.01942578125).build());
        parameters.add(new Parameter.Builder("TrqSen2Out", "2104", new int[] {0}, "В", "V").multiplier(0.01942578125).build());
        parameters.add(new Parameter.Builder("TrqSen3Out", "2105", new int[] {0}, "В", "V").multiplier(0.01942578125).build());
        parameters.add(new Parameter.Builder("MotVehVcity", "2107", new int[] {0}, "kph", "kph").build());
        parameters.add(new Parameter.Builder("EngRevol", "210A", new int[] {0}, "rpm", "rpm").multiplier(32.0).build());
        parameters.add(new Parameter.Builder("MotCurr", "210C", new int[] {0}, "A", "A").build());
        parameters.add(new Parameter.Builder("CommCurr", "210D", new int[] {0}, "A", "A").build());
        parameters.add(new Parameter.Builder("ThermistorTemp", "2113", new int[] {0}, "°С", "°С").offset(-50.0).build());
        parameters.add(new Parameter.Builder("PIGPower", "2115", new int[] {0}, "В", "V").multiplier(0.1).build());
        parameters.add(new Parameter.Builder("IGPower", "2116", new int[] {0}, "В", "V").multiplier(0.1).build());
        parameters.add(new Parameter.Builder("TRQ1Value", "2118", new int[] {0}, "В", "V").multiplier(0.01942578125).build());
        parameters.add(new Parameter.Builder("TRQ2Value", "2119", new int[] {0}, "В", "V").multiplier(0.01942578125).build());
        parameters.add(new Parameter.Builder("TRQ3Value", "211A", new int[] {0}, "В", "V").multiplier(0.01942578125).build());
        parameters.add(new Parameter.Builder("MotVoltsPos", "211B", new int[] {0}, "В", "V").multiplier(0.1).build());
        parameters.add(new Parameter.Builder("MotVoltsNeg", "211C", new int[] {0}, "В", "V").multiplier(0.1).build());
        parameters.add(new Parameter.Builder("IG_ON_OFF_Time", "211F", new int[] {0}, "times", "times").build());
        parameters.add(new Parameter.Builder("NumDTC", "21E1", new int[] {0}, "pieces", "pieces").build());
        ToyotaEMPS.super.setParameters(parameters);
    }
}
