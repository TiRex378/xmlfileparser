package tirex.ezway.libs.ecu;

import tirex.ezway.libs.Parameter;

import java.util.ArrayList;

public class micas_11_E2 extends DefaultAuto {
    public micas_11_E2() {
        ArrayList<Parameter> parameters = new ArrayList<Parameter>();
        parameters.add(new Parameter.Builder("RPM", "2101", new int[] {2, 3}, "rpm", "rpm").multiplier(0.5).wordLE(true).build());
        parameters.add(new Parameter.Builder("CoolantTemp", "2101", new int[] {4, 5}, "°С", "°С").multiplier(0.0078125).wordLE(true).build());
        parameters.add(new Parameter.Builder("IntakeAirTemp", "2101", new int[] {6, 7}, "°С", "°С").multiplier(0.0078125).wordLE(true).build());
        parameters.add(new Parameter.Builder("ThrottlePosition", "2101", new int[] {8, 9}, "%", "%").multiplier(0.0078125).wordLE(true).build());
        parameters.add(new Parameter.Builder("InjectorPulseWidth", "2101", new int[] {18, 19}, "ms", "ms").multiplier(0.005).wordLE(true).build());
        parameters.add(new Parameter.Builder("LitersPerHour", "2101", new int[] {20, 21}, "lphour", "lphour").multiplier(0.00390625).wordLE(true).build());
        parameters.add(new Parameter.Builder("MAF", "2101", new int[] {22, 23}, "kgperhour", "kgperhour").multiplier(0.0625).wordLE(true).build());
        parameters.add(new Parameter.Builder("RDV", "2101", new int[] {26, 27}, "%", "%").multiplier(0.0078125).wordLE(true).build());
        parameters.add(new Parameter.Builder("Speed", "2101", new int[] {41, 42}, "kph", "kph").wordLE(true).build());
        parameters.add(new Parameter.Builder("TimingAdvance", "2101", new int[] {53, 54}, "degree", "degree").multiplier(0.75).wordLE(true).build());
        parameters.add(new Parameter.Builder("ADC_SystemVoltage", "2103", new int[] {2, 3}, "В", "V").multiplier(0.00390625).wordLE(true).build());
        parameters.add(new Parameter.Builder("ADC_CoolantTemp", "2103", new int[] {4, 5}, "В", "V").multiplier(0.004885).wordLE(true).build());
        parameters.add(new Parameter.Builder("ADC_IntakeAirTemp", "2103", new int[] {6, 7}, "В", "V").multiplier(0.004885).wordLE(true).build());
        parameters.add(new Parameter.Builder("ADC_MAF", "2103", new int[] {8, 9}, "В", "V").multiplier(0.004885).wordLE(true).build());
        parameters.add(new Parameter.Builder("ADC_ThrottlePosition", "2103", new int[] {10, 11}, "В", "V").multiplier(0.004885).wordLE(true).build());
        parameters.add(new Parameter.Builder("ADC_Detonation", "Core21E2", new int[] {12, 13}, "В", "V").multiplier(9.76577E-4).wordLE(true).build());
        parameters.add(new Parameter.Builder("ADC_Oxygen_b1s1", "2103", new int[] {14, 15}, "В", "V").multiplier(0.004885).wordLE(true).build());
        parameters.add(new Parameter.Builder("ADC_Oxygen_b1s1_OM", "2103", new int[] {16, 17}, "Om", "Om").multiplier(4.0).wordLE(true).build());
        micas_11_E2.super.setParameters(parameters);
    }
}
