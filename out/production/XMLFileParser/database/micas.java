package tirex.ezway.libs.ecu;

import tirex.ezway.libs.Parameter;

import java.util.ArrayList;

public class micas extends DefaultAuto {
    public micas() {
        ArrayList<Parameter> parameters = new ArrayList<Parameter>();
        parameters.add(new Parameter.Builder("FuelSystemStatus_Trim", "2101", new int[] {14}, "бит", "bit").build());
        parameters.add(new Parameter.Builder("RPM", "2101", new int[] {18, 19}, "rpm", "rpm").build());
        parameters.add(new Parameter.Builder("ThrottlePosition", "2101", new int[] {20, 21}, "%", "%").multiplier(0.1).build());
        parameters.add(new Parameter.Builder("TimingAdvance", "2101", new int[] {22, 23}, "°", "°").multiplier(0.1).build());
        parameters.add(new Parameter.Builder("InjectorPulseWidth", "2101", new int[] {24, 25}, "ms", "ms").multiplier(0.01).build());
        parameters.add(new Parameter.Builder("MAF", "2101", new int[] {26, 27}, "kgperhour", "kgperhour").multiplier(0.1).build());
        parameters.add(new Parameter.Builder("IntakeManifoldPressure", "2101", new int[] {28, 29}, "torr", "torr").build());
        parameters.add(new Parameter.Builder("CoolantTemp", "2101", new int[] {30}, "°С", "°С").offset(-40.0).build());
        parameters.add(new Parameter.Builder("IntakeAirTemp", "2101", new int[] {31}, "°С", "°С").offset(-40.0).build());
        parameters.add(new Parameter.Builder("LitersPerHour", "2101", new int[] {32, 33}, "lphour", "lphour").multiplier(0.01).build());
        parameters.add(new Parameter.Builder("LitersPerKilometer", "2101", new int[] {34, 35}, "lph", "lph").multiplier(0.1).build());
        parameters.add(new Parameter.Builder("Speed", "2101", new int[] {36}, "kph", "kph").build());
        parameters.add(new Parameter.Builder("ControlModuleVoltage", "2101", new int[] {37}, "В", "V").multiplier(0.1).build());
        parameters.add(new Parameter.Builder("Rxx", "2101", new int[] {38}, "шаг", "step").build());
        parameters.add(new Parameter.Builder("RPMSetup", "2101", new int[] {39, 40}, "rpm", "rpm").build());
        parameters.add(new Parameter.Builder("Oxygen_b1s1", "2101", new int[] {41}, "В", "V").multiplier(0.0048828125).build());
        micas.super.setParameters(parameters);
    }
}
