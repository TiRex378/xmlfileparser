package tirex.ezway.libs.ecu;

import tirex.ezway.libs.Parameter;

import java.util.ArrayList;

public class UAZ_EDC16C39 extends DefaultAuto {
    public UAZ_EDC16C39() {
        ArrayList<Parameter> parameters = new ArrayList<Parameter>();
        parameters.add(new Parameter.Builder("RPMXX", "211E", new int[] {0}, "rpm", "rpm").multiplier(0.001).build());
        parameters.add(new Parameter.Builder("FuelPressure", "2103", new int[] {0, 1}, "kpa", "kpa").multiplier(10.0).build());
        parameters.add(new Parameter.Builder("FuelPressureDeflection", "215F", new int[] {0, 1}, "кПа", "kPa").multiplier(10.0).build());
        parameters.add(new Parameter.Builder("FuelPressureSet", "2164", new int[] {0, 1}, "кПа", "kPa").multiplier(10.0).build());
        parameters.add(new Parameter.Builder("F_BrakePedal2", "210E", new int[] {0}, "бит", "bit").build());
        parameters.add(new Parameter.Builder("F_ClutchPedal", "210F", new int[] {0}, "бит", "bit").build());
        parameters.add(new Parameter.Builder("F_15ElectricalSystem", "2110", new int[] {0}, "бит", "bit").build());
        parameters.add(new Parameter.Builder("MAF", "2111", new int[] {0}, "kgperhour", "kgperhour").multiplier(0.00634).build());
        parameters.add(new Parameter.Builder("F_GlowPlugsLED", "2112", new int[] {0}, "бит", "bit").build());
        parameters.add(new Parameter.Builder("F_GlowPlugsRelay", "2113", new int[] {0}, "бит", "bit").build());
        parameters.add(new Parameter.Builder("PedalPositionVolt1", "2114", new int[] {0}, "mВ", "mV").multiplier(0.02441406).build());
        parameters.add(new Parameter.Builder("PedalPositionVolt2", "2115", new int[] {0}, "mВ", "mV").multiplier(0.02441406).build());
        parameters.add(new Parameter.Builder("PedalPosition", "2116", new int[] {0}, "%", "%").multiplier(0.3921568).build());
        parameters.add(new Parameter.Builder("BaroPressure", "2117", new int[] {0}, "кПа", "kPa").multiplier(10.0).build());
        parameters.add(new Parameter.Builder("BaroPressureVolt", "2118", new int[] {0}, "mВ", "mV").multiplier(0.02441406).build());
        parameters.add(new Parameter.Builder("CoolantTempVolt", "211B", new int[] {0}, "mВ", "mV").multiplier(0.02441406).build());
        parameters.add(new Parameter.Builder("CoolantTemp", "211C", new int[] {0}, "°С", "°С").offset(-40.0).build());
        parameters.add(new Parameter.Builder("FuelTemp", "2197", new int[] {0}, "°С", "°С").offset(-40.0).build());
        parameters.add(new Parameter.Builder("ControlModuleVoltage", "211D", new int[] {0}, "В", "V").multiplier(0.1).build());
        parameters.add(new Parameter.Builder("Speed", "211F", new int[] {0}, "kph", "kph").build());
        parameters.add(new Parameter.Builder("CycleConsumption", "2120", new int[] {0}, "mgpc", "mgpc").build());
        parameters.add(new Parameter.Builder("F_FuelSystemStatus_Immo", "2121", new int[] {0}, "бит", "bit").build());
        parameters.add(new Parameter.Builder("SCTNVD", "213A", new int[] {0}, "мА", "mA").build());
        parameters.add(new Parameter.Builder("QTNVD", "213B", new int[] {0, 1}, "%", "%").multiplier(0.00521).build());
        parameters.add(new Parameter.Builder("ICTNVD", "213C", new int[] {0, 1}, "мА", "mA").build());
        parameters.add(new Parameter.Builder("S_SIN", "2159", new int[] {0}, "бит", "bit").build());
        parameters.add(new Parameter.Builder("FuelTempVolt", "215A", new int[] {0, 1}, "mВ", "mV").multiplier(0.02441406).build());
        parameters.add(new Parameter.Builder("S_INJ", "215B", new int[] {0}, "бит. маска", "бит. маска").build());
        parameters.add(new Parameter.Builder("FuelPressureVolt", "2167", new int[] {0, 1}, "mВ", "mV").multiplier(0.02441406).build());
        parameters.add(new Parameter.Builder("F_BrakePedal1", "2168", new int[] {0}, "бит", "bit").build());
        parameters.add(new Parameter.Builder("F_BrakePedal", "2186", new int[] {0}, "бит", "bit").build());
        parameters.add(new Parameter.Builder("EngineTorque", "218A", new int[] {0}, "Nm", "Nm").multiplier(2.0).offset(-100.0).build());
        parameters.add(new Parameter.Builder("EngineTorqueperMax", "218C", new int[] {0}, "%", "%").multiplier(0.3921568).build());
        parameters.add(new Parameter.Builder("S_IMMO", "218D", new int[] {0}, "бит", "bit").build());
        parameters.add(new Parameter.Builder("S_OBD", "218E", new int[] {0}, "бит", "bit").build());
        parameters.add(new Parameter.Builder("S_EDC", "218F", new int[] {0}, "бит", "bit").build());
        parameters.add(new Parameter.Builder("S_WATER", "2190", new int[] {0}, "бит", "bit").build());
        parameters.add(new Parameter.Builder("QTMOT", "2191", new int[] {0}, "%", "%").build());
        parameters.add(new Parameter.Builder("QEGR", "2192", new int[] {0}, "%", "%").multiplier(0.3921568).build());
        parameters.add(new Parameter.Builder("B_ACC", "2193", new int[] {0}, "бит", "bit").build());
        parameters.add(new Parameter.Builder("CyclicAirflow", "2194", new int[] {0}, "мг/ц", "мг/ц").multiplier(10.0).build());
        parameters.add(new Parameter.Builder("QAIR", "2195", new int[] {0}, "ms", "ms").build());
        parameters.add(new Parameter.Builder("FD_GLOW", "2196", new int[] {0, 1}, "бит", "bit").build());
        parameters.add(new Parameter.Builder("EngineTorquePedal", "219A", new int[] {0}, "Нм", "Нм").multiplier(10.0).build());
        parameters.add(new Parameter.Builder("ZTRG", "21B3", new int[] {0, 1}, "%", "%").build());
        parameters.add(new Parameter.Builder("IntakeAirTemp", "21B6", new int[] {0, 1}, "°С", "°С").offset(-40.0).build());
        parameters.add(new Parameter.Builder("R_FAN", "21B7", new int[] {0, 1}, "%", "%").build());
        parameters.add(new Parameter.Builder("ACC", "2104", new int[] {0}, "бит", "bit").build());
        parameters.add(new Parameter.Builder("PACC", "210A", new int[] {0}, "кПа", "kPa").multiplier(0.0015259).build());
        parameters.add(new Parameter.Builder("UPACC", "210C", new int[] {0}, "mВ", "mV").multiplier(0.02441406).build());
        UAZ_EDC16C39.super.setParameters(parameters);
    }
}
