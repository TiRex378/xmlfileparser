package tirex.ezway.libs.ecu;

import tirex.ezway.libs.Parameter;

import java.util.ArrayList;

public class Hyundai_G4FC extends DefaultAuto {
    public Hyundai_G4FC() {
        ArrayList<Parameter> parameters = new ArrayList<Parameter>();
        parameters.add(new Parameter.Builder("ControlModuleVoltage", "2101", new int[] {7}, "В", "V").multiplier(0.0678515625).build());
        parameters.add(new Parameter.Builder("ControlModuleVoltageAft", "2101", new int[] {8}, "В", "V").multiplier(0.0678515625).build());
        parameters.add(new Parameter.Builder("RPM", "2101", new int[] {10, 11}, "rpm", "rpm").multiplier(0.25).wordLE(true).build());
        parameters.add(new Parameter.Builder("JRPMSxx", "2101", new int[] {12}, "rpm", "rpm").multiplier(10.0).build());
        parameters.add(new Parameter.Builder("MAP_Voltage", "2101", new int[] {17, 18}, "В", "V").multiplier(0.0048828125).wordLE(true).build());
        parameters.add(new Parameter.Builder("IntakeManifoldPressure", "2101", new int[] {19, 20}, "кПа", "kPa").multiplier(0.00390625).wordLE(true).build());
        parameters.add(new Parameter.Builder("CoolantTempVoltage", "2101", new int[] {26, 27}, "В", "V").multiplier(0.0048828125).wordLE(true).build());
        parameters.add(new Parameter.Builder("CoolantTemp", "2101", new int[] {28}, "°С", "°С").multiplier(0.75).offset(-48.0).build());
        parameters.add(new Parameter.Builder("IntakeAirTempVoltage", "2101", new int[] {34, 35}, "В", "V").multiplier(0.0048828125).wordLE(true).build());
        parameters.add(new Parameter.Builder("IntakeAirTemp", "2101", new int[] {36}, "°С", "°С").multiplier(0.75).offset(-48.0).build());
        parameters.add(new Parameter.Builder("EngineOilTemp", "2101", new int[] {37}, "°С", "°С").multiplier(0.75).offset(-48.0).build());
        parameters.add(new Parameter.Builder("Oxygen_b1s1", "2102", new int[] {7}, "В", "V").multiplier(0.00521).offset(-0.2).build());
        parameters.add(new Parameter.Builder("Oxygen_b1s2", "2102", new int[] {9}, "В", "V").multiplier(0.00521).offset(-0.2).build());
        parameters.add(new Parameter.Builder("Speed", "2102", new int[] {15}, "kph", "kph").multiplier(1.25).build());
        parameters.add(new Parameter.Builder("InjectorPulseWidth", "2102", new int[] {25, 26}, "ms", "ms").multiplier(0.8192).wordLE(true).build());
        parameters.add(new Parameter.Builder("InjectorPulseWidth2", "2102", new int[] {27, 28}, "ms", "ms").multiplier(0.8192).wordLE(true).build());
        parameters.add(new Parameter.Builder("InjectorPulseWidth3", "2102", new int[] {29, 30}, "ms", "ms").multiplier(0.8192).wordLE(true).build());
        parameters.add(new Parameter.Builder("InjectorPulseWidth4", "2102", new int[] {31, 32}, "ms", "ms").multiplier(0.8192).wordLE(true).build());
        parameters.add(new Parameter.Builder("TimingAdvance", "2113", new int[] {7}, "degree", "degree").multiplier(0.75).build());
        parameters.add(new Parameter.Builder("TimingAdvance2", "2113", new int[] {8}, "degree", "degree").multiplier(0.75).build());
        parameters.add(new Parameter.Builder("TimingAdvance3", "2113", new int[] {9}, "degree", "degree").multiplier(0.75).build());
        parameters.add(new Parameter.Builder("TimingAdvance4", "2113", new int[] {10}, "degree", "degree").multiplier(0.75).build());
        parameters.add(new Parameter.Builder("IntakeCoolantTemp", "2113", new int[] {30}, "°С", "°С").multiplier(0.75).offset(-48.0).build());
        parameters.add(new Parameter.Builder("BaroPressure", "2113", new int[] {42}, "кПа", "kPa").multiplier(0.5).build());
        parameters.add(new Parameter.Builder("Core15_9w", "2115", new int[] {9, 10}, "%", "%").multiplier(0.046875).wordLE(true).build());
        parameters.add(new Parameter.Builder("Core17_7", "2117", new int[] {7}, "degree", "degree").multiplier(0.5).build());
        parameters.add(new Parameter.Builder("Core17_8w", "2117", new int[] {8, 9}, "degree", "degree").multiplier(0.0078125).wordLE(true).build());
        Hyundai_G4FC.super.setParameters(parameters);
    }
}
