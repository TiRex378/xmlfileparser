package tirex.ezway.libs.ecu;

import tirex.ezway.libs.Parameter;

import java.util.ArrayList;

public class ford extends DefaultAuto {
    public ford() {
        ArrayList<Parameter> parameters = new ArrayList<Parameter>();
        parameters.add(new Parameter.Builder("LFW_ABS", "223987", new int[] {3}, "kph", "kph").build());
        parameters.add(new Parameter.Builder("RFW_ABS", "223987", new int[] {4}, "kph", "kph").build());
        parameters.add(new Parameter.Builder("LRW_ABS", "223987", new int[] {5}, "kph", "kph").build());
        parameters.add(new Parameter.Builder("RRW_ABS", "223987", new int[] {6}, "kph", "kph").build());
        parameters.add(new Parameter.Builder("LFW", "223988", new int[] {1}, "kph", "kph").build());
        parameters.add(new Parameter.Builder("RFW", "223989", new int[] {1}, "kph", "kph").build());
        parameters.add(new Parameter.Builder("LFW_4WD", "223988", new int[] {1}, "kph", "kph").build());
        parameters.add(new Parameter.Builder("RFW_4WD", "223989", new int[] {1}, "kph", "kph").build());
        parameters.add(new Parameter.Builder("LRW_4WD", "22398A", new int[] {1}, "kph", "kph").build());
        parameters.add(new Parameter.Builder("RRW_4WD", "22398B", new int[] {1}, "kph", "kph").build());
        parameters.add(new Parameter.Builder("RWClutch", "22D128", new int[] {1}, "%", "%").multiplier(0.392157).build());
        parameters.add(new Parameter.Builder("TFT", "221E1C", new int[] {1, 2}, "fahrenheit", "fahrenheit").multiplier(0.125).build());
        parameters.add(new Parameter.Builder("LFT", "220007", new int[] {1}, "%", "%").multiplier(0.78125).offset(-100.0).build());
        parameters.add(new Parameter.Builder("IntAirTemp", "22000F", new int[] {1}, "°С", "°С").offset(-40.0).build());
        parameters.add(new Parameter.Builder("CAT", "22003C", new int[] {1, 2}, "°С", "°С").multiplier(0.1).offset(-40.0).build());
        parameters.add(new Parameter.Builder("MAPvolt", "220900", new int[] {1, 2}, "В", "V").build());
        parameters.add(new Parameter.Builder("AcceleratorPedal", "2209D4", new int[] {1}, "%", "%").multiplier(0.5).build());
        parameters.add(new Parameter.Builder("IntAirTemp1123", "221123", new int[] {1}, "°С", "°С").build());
        parameters.add(new Parameter.Builder("BarometricPressure", "221127", new int[] {1}, "inhg", "inhg").multiplier(0.125).build());
        parameters.add(new Parameter.Builder("CoolantTempAlt", "221139", new int[] {1}, "°С", "°С").build());
        parameters.add(new Parameter.Builder("LFT1", "221156", new int[] {1}, "%", "%").build());
        parameters.add(new Parameter.Builder("CHT", "221624", new int[] {1, 2}, "fahrenheit", "fahrenheit").multiplier(2.0).build());
        parameters.add(new Parameter.Builder("CHT1", "220334", new int[] {1, 2}, "°С", "°С").multiplier(0.015625).build());
        parameters.add(new Parameter.Builder("MAF_Alt", "221671", new int[] {1, 2}, "grpersec", "grpersec").multiplier(0.01).build());
        parameters.add(new Parameter.Builder("FRP", "22168C", new int[] {1, 2}, "psi", "psi").multiplier(0.0078125).build());
        parameters.add(new Parameter.Builder("FRT", "22168E", new int[] {1}, "fahrenheit", "fahrenheit").multiplier(2.0).build());
        parameters.add(new Parameter.Builder("ECUFuelLevelInd", "2216C1", new int[] {1, 2}, "%", "%").multiplier(0.00304878).build());
        parameters.add(new Parameter.Builder("EvC", "22990A", new int[] {1}, "°С", "°С").offset(-40.0).build());
        parameters.add(new Parameter.Builder("EvCVolt", "22996E", new int[] {1}, "В", "V").multiplier(9.77E-4).build());
        parameters.add(new Parameter.Builder("AirTemp", "0146", new int[] {0}, "°С", "°С").offset(-40.0).build());
        parameters.add(new Parameter.Builder("TireSensorLF", "224140", new int[] {3, 4}, "кПа", "kPa").multiplier(0.34475).build());
        parameters.add(new Parameter.Builder("TireSensorRF", "224140", new int[] {5, 6}, "кПа", "kPa").multiplier(0.34475).build());
        parameters.add(new Parameter.Builder("TireSensorLR", "224141", new int[] {5, 6}, "кПа", "kPa").multiplier(0.34475).build());
        parameters.add(new Parameter.Builder("TireSensorRR", "224141", new int[] {3, 4}, "кПа", "kPa").multiplier(0.34475).build());
        parameters.add(new Parameter.Builder("TireLastTemp", "224160", new int[] {1}, "°С", "°С").offset(-40.0).build());
        ford.super.setParameters(parameters);
    }
}
