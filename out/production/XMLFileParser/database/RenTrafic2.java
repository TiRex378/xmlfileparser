package tirex.ezway.libs.ecu;

import tirex.ezway.libs.Parameter;

import java.util.ArrayList;

public class RenTrafic2 extends DefaultAuto {
    public RenTrafic2() {
        ArrayList<Parameter> parameters = new ArrayList<Parameter>();
        parameters.add(new Parameter.Builder("RPM", "21AA", new int[] {14, 15}, "rpm", "rpm").build());
        parameters.add(new Parameter.Builder("Speed", "21AA", new int[] {16, 17}, "kph", "kph").multiplier(0.01).build());
        parameters.add(new Parameter.Builder("CoolantTemp", "21A9", new int[] {2, 3}, "°С", "°С").multiplier(0.1).offset(-273.0).build());
        parameters.add(new Parameter.Builder("IntakeAirTemp", "21A9", new int[] {4, 5}, "°С", "°С").multiplier(0.1).offset(-273.0).build());
        parameters.add(new Parameter.Builder("FuelTemp", "21A9", new int[] {6, 7}, "°С", "°С").multiplier(0.1).offset(-273.0).build());
        parameters.add(new Parameter.Builder("BaroPressure", "21A9", new int[] {8, 9}, "hPa", "hPa").build());
        parameters.add(new Parameter.Builder("BoostPressure", "21A9", new int[] {10, 11}, "hPa", "hPa").build());
        parameters.add(new Parameter.Builder("FuelRailPressure_diesel", "21A9", new int[] {12, 13}, "hPa", "hPa").multiplier(100.0).build());
        parameters.add(new Parameter.Builder("MAF", "21A9", new int[] {14, 15}, "kgperhour", "kgperhour").multiplier(0.1).build());
        parameters.add(new Parameter.Builder("ControlModuleVoltage", "21A9", new int[] {18, 19}, "В", "V").multiplier(0.0204).build());
        parameters.add(new Parameter.Builder("Oxygen_b1s1", "21A9", new int[] {20, 21}, "В", "V").multiplier(0.01766).build());
        parameters.add(new Parameter.Builder("Oxygen_b1s2", "21A9", new int[] {22, 23}, "В", "V").multiplier(0.01766).build());
        parameters.add(new Parameter.Builder("InjectorPulseWidth", "21AB", new int[] {4, 5}, "ms", "ms").multiplier(0.005).build());
        parameters.add(new Parameter.Builder("CycleConsumption", "21AB", new int[] {4, 5}, "mmpc", "mmpc").multiplier(0.01).build());
        parameters.add(new Parameter.Builder("EngineTorque", "21A3", new int[] {16}, "Nm", "Nm").multiplier(2.0).offset(-100.0).build());
        RenTrafic2.super.setParameters(parameters);
    }
}
