package tirex.ezway.libs.ecu;

import tirex.ezway.libs.Parameter;

import java.util.ArrayList;

public class RenMegane extends DefaultAuto {
    public RenMegane() {
        ArrayList<Parameter> parameters = new ArrayList<Parameter>();
        parameters.add(new Parameter.Builder("RPM", "21A9", new int[] {2, 3}, "rpm", "rpm").build());
        parameters.add(new Parameter.Builder("ControlModuleVoltage", "21A9", new int[] {4}, "В", "V").multiplier(0.032).offset(8.0).build());
        parameters.add(new Parameter.Builder("IntakeManifoldPressure", "21A9", new int[] {5}, "кПа", "kPa").multiplier(0.37).offset(10.3).build());
        parameters.add(new Parameter.Builder("ThrottlePosition", "21A9", new int[] {6}, "%", "%").multiplier(0.4).build());
        parameters.add(new Parameter.Builder("CoolantTemp", "21A9", new int[] {7}, "°С", "°С").multiplier(0.625).offset(-40.0).build());
        parameters.add(new Parameter.Builder("IntakeAirTemp", "21A9", new int[] {8}, "°С", "°С").multiplier(0.625).offset(-40.0).build());
        parameters.add(new Parameter.Builder("Oxygen_b1s1", "21A9", new int[] {9}, "В", "V").multiplier(0.00976).build());
        parameters.add(new Parameter.Builder("Oxygen_b1s2", "21A9", new int[] {10}, "В", "V").multiplier(0.00976).build());
        parameters.add(new Parameter.Builder("Speed", "21A9", new int[] {11}, "kph", "kph").build());
        parameters.add(new Parameter.Builder("BaroPressure", "21A9", new int[] {12, 13}, "кПа", "kPa").multiplier(-0.37).offset(104.35).build());
        parameters.add(new Parameter.Builder("InjectorPulseWidth", "21AA", new int[] {7, 8}, "ms", "ms").multiplier(0.004).build());
        parameters.add(new Parameter.Builder("LitersPerHour", "21AA", new int[] {12, 13}, "lphour", "lphour").multiplier(0.01).build());
        RenMegane.super.setParameters(parameters);
    }
}
