package tirex.ezway.libs.ecu;

import tirex.ezway.libs.Parameter;

import java.util.ArrayList;

public class opel_Y17DT extends DefaultAuto {
    public opel_Y17DT() {
        ArrayList<Parameter> parameters = new ArrayList<Parameter>();
        parameters.add(new Parameter.Builder("ControlModuleVoltage", "Core", new int[] {32}, "В", "V").multiplier(0.25).build());
        parameters.add(new Parameter.Builder("MAF_Voltage", "Core", new int[] {47}, "В", "V").multiplier(0.01953125).build());
        parameters.add(new Parameter.Builder("MAF", "Core", new int[] {48, 49}, "kgperhour", "kgperhour").multiplier(0.025).build());
        parameters.add(new Parameter.Builder("BoostPressure", "Core", new int[] {51}, "кПа", "kPa").build());
        parameters.add(new Parameter.Builder("BoostPressureSet", "Core", new int[] {52}, "кПа", "kPa").build());
        parameters.add(new Parameter.Builder("BaroPressure", "Core", new int[] {69}, "кПа", "kPa").build());
        parameters.add(new Parameter.Builder("IntakeAirTemp", "Core", new int[] {45}, "°С", "°С").multiplier(0.75).offset(-40.0).build());
        parameters.add(new Parameter.Builder("CoolantTemp", "Core", new int[] {43}, "°С", "°С").multiplier(0.75).offset(-40.0).build());
        parameters.add(new Parameter.Builder("FuelTemp", "Core", new int[] {65}, "°С", "°С").multiplier(0.75).offset(-40.0).build());
        parameters.add(new Parameter.Builder("CycleConsumption", "Core", new int[] {66}, "mgpc", "mgpc").build());
        parameters.add(new Parameter.Builder("RPMPump", "Core", new int[] {35, 36}, "rpm", "rpm").multiplier(0.0625).build());
        parameters.add(new Parameter.Builder("RPM", "Core", new int[] {33, 34}, "rpm", "rpm").multiplier(0.125).build());
        parameters.add(new Parameter.Builder("RPMSet", "Core", new int[] {38}, "rpm", "rpm").build());
        parameters.add(new Parameter.Builder("Speed", "Core", new int[] {39}, "kph", "kph").build());
        parameters.add(new Parameter.Builder("AirTemp", "Core", new int[] {46}, "°С", "°С").multiplier(0.75).offset(-40.0).build());
        opel_Y17DT.super.setParameters(parameters);
    }
}
