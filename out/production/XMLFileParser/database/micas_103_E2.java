package tirex.ezway.libs.ecu;

import tirex.ezway.libs.Parameter;

import java.util.ArrayList;

public class micas_103_E2 extends DefaultAuto {
    public micas_103_E2() {
        ArrayList<Parameter> parameters = new ArrayList<Parameter>();
        parameters.add(new Parameter.Builder("ADC_Detonation", "21E2", new int[] {2, 3}, "В", "V").multiplier(0.004885).build());
        parameters.add(new Parameter.Builder("ADC_CoolantTemp", "21E2", new int[] {12, 13}, "В", "V").multiplier(0.004885).build());
        parameters.add(new Parameter.Builder("ADC_Oxygen_b1s1", "21E2", new int[] {14, 15}, "В", "V").multiplier(0.004885).build());
        parameters.add(new Parameter.Builder("ADC_MAP", "21E2", new int[] {16, 17}, "В", "V").multiplier(0.004885).build());
        parameters.add(new Parameter.Builder("ADC_Oxygen_b1s2", "21E2", new int[] {18, 19}, "В", "V").multiplier(0.004885).build());
        parameters.add(new Parameter.Builder("ADC_ThrottlePosition", "21E2", new int[] {20, 21}, "В", "V").multiplier(0.004885).build());
        micas_103_E2.super.setParameters(parameters);
    }
}
