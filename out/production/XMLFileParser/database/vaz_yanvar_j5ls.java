package tirex.ezway.libs.ecu;

import tirex.ezway.libs.Parameter;

import java.util.ArrayList;

public class vaz_yanvar_j5ls extends DefaultAuto {
    public vaz_yanvar_j5ls() {
        ArrayList<Parameter> parameters = new ArrayList<Parameter>();
        parameters.add(new Parameter.Builder("CoolantTemp", "210F", new int[] {10}, "°С", "°С").offset(-40.0).build());
        parameters.add(new Parameter.Builder("ThrottlePosition", "210F", new int[] {13}, "%", "%").build());
        parameters.add(new Parameter.Builder("RPM", "210F", new int[] {14}, "rpm", "rpm").multiplier(40.0).build());
        parameters.add(new Parameter.Builder("RPMXX", "210F", new int[] {14}, "rpm", "rpm").multiplier(10.0).build());
        parameters.add(new Parameter.Builder("SSMxx", "210F", new int[] {18}, "шаг", "step").build());
        parameters.add(new Parameter.Builder("FSMxx", "210F", new int[] {19}, "шаг", "step").build());
        parameters.add(new Parameter.Builder("TimingAdvance", "210F", new int[] {22}, "degree", "degree").multiplier(0.5).build());
        parameters.add(new Parameter.Builder("ADC_MAF", "210F", new int[] {28}, "В", "V").multiplier(0.01953125).build());
        parameters.add(new Parameter.Builder("ADC_SystemVoltage", "210F", new int[] {29}, "В", "V").multiplier(0.090907).build());
        parameters.add(new Parameter.Builder("ADC_ThrottlePosition", "210F", new int[] {31}, "В", "V").multiplier(0.01953125).build());
        parameters.add(new Parameter.Builder("ADC_CoolantTemp", "210F", new int[] {32}, "В", "V").multiplier(0.01953125).build());
        parameters.add(new Parameter.Builder("ADC_IntakeAirTemp", "210F", new int[] {33}, "В", "V").multiplier(0.01953125).build());
        parameters.add(new Parameter.Builder("ADC_Oxygen_b1s1", "210F", new int[] {34, 35}, "В", "V").multiplier(0.01953125).build());
        parameters.add(new Parameter.Builder("Speed", "210F", new int[] {37}, "kph", "kph").build());
        parameters.add(new Parameter.Builder("JRPMSxx", "210F", new int[] {38}, "rpm", "rpm").multiplier(10.0).build());
        parameters.add(new Parameter.Builder("IntakeAirTemp", "210F", new int[] {39}, "°С", "°С").offset(-40.0).build());
        parameters.add(new Parameter.Builder("InjectorPulseWidth", "210F", new int[] {45, 46}, "ms", "ms").multiplier(0.008).wordLE(true).build());
        parameters.add(new Parameter.Builder("MAF", "210F", new int[] {47, 48}, "kgperhour", "kgperhour").multiplier(0.1).wordLE(true).build());
        parameters.add(new Parameter.Builder("CyclicAirflow", "210F", new int[] {49, 50}, "mgpc", "mgpc").multiplier(0.16666667).wordLE(true).build());
        parameters.add(new Parameter.Builder("FuelSystemStatus_Trim", "2101", new int[] {1}, "бит", "bit").build());
        vaz_yanvar_j5ls.super.setParameters(parameters);
    }
}
