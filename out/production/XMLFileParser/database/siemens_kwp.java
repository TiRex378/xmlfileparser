package tirex.ezway.libs.ecu;

import tirex.ezway.libs.Parameter;

import java.util.ArrayList;

public class siemens_kwp extends DefaultAuto {
    public siemens_kwp() {
        ArrayList<Parameter> parameters = new ArrayList<Parameter>();
        parameters.add(new Parameter.Builder("ControlModuleVoltage", "2101", new int[] {2}, "В", "V").multiplier(0.1016).build());
        parameters.add(new Parameter.Builder("CoolantTempVoltage", "2101", new int[] {4}, "В", "V").multiplier(0.01953125).build());
        parameters.add(new Parameter.Builder("CoolantTemp", "2101", new int[] {5}, "°С", "°С").multiplier(0.75).offset(-48.0).build());
        parameters.add(new Parameter.Builder("IntakeAirTempVoltage", "2101", new int[] {6}, "В", "V").multiplier(0.01953125).build());
        parameters.add(new Parameter.Builder("IntakeAirTemp", "2101", new int[] {7}, "°С", "°С").multiplier(0.75).offset(-48.0).build());
        parameters.add(new Parameter.Builder("ThrottlePositionVoltage", "2101", new int[] {8}, "В", "V").multiplier(0.01953125).build());
        parameters.add(new Parameter.Builder("ThrottlePosition", "2101", new int[] {9}, "%", "%").multiplier(0.4686).build());
        parameters.add(new Parameter.Builder("TPS_AD_MMV_IS", "2101", new int[] {10, 11}, "%", "%").multiplier(0.0018252).wordLE(true).build());
        parameters.add(new Parameter.Builder("MAPvolt", "2101", new int[] {12}, "В", "V").multiplier(0.01953125).build());
        parameters.add(new Parameter.Builder("MAF", "2101", new int[] {13, 14}, "kgperhour", "kgperhour").multiplier(0.03125).wordLE(true).build());
        parameters.add(new Parameter.Builder("IntakeManifoldPressure", "2101", new int[] {15, 16}, "кПа", "kPa").multiplier(0.00829175).wordLE(true).build());
        parameters.add(new Parameter.Builder("CyclicAirflow", "2101", new int[] {17, 18}, "mgpc", "mgpc").multiplier(0.02119478).wordLE(true).build());
        parameters.add(new Parameter.Builder("Speed", "2101", new int[] {20}, "kph", "kph").build());
        parameters.add(new Parameter.Builder("RPM", "2101", new int[] {21, 22}, "rpm", "rpm").wordLE(true).build());
        parameters.add(new Parameter.Builder("JRPMSxx", "2101", new int[] {23, 24}, "rpm", "rpm").wordLE(true).build());
        parameters.add(new Parameter.Builder("Oxygen_b1s1", "2101", new int[] {27, 28}, "В", "V").multiplier(0.0048828125).wordLE(true).build());
        parameters.add(new Parameter.Builder("Oxygen_b1s2", "2101", new int[] {29, 30}, "В", "V").multiplier(0.0048828125).wordLE(true).build());
        parameters.add(new Parameter.Builder("TimingAdvance", "2101", new int[] {31}, "degree", "degree").multiplier(-0.375).offset(72.0).build());
        parameters.add(new Parameter.Builder("TimingAdvance_1", "2101", new int[] {32}, "degree", "degree").multiplier(-0.375).offset(72.0).build());
        parameters.add(new Parameter.Builder("TimingAdvance_2", "2101", new int[] {33}, "degree", "degree").multiplier(-0.375).offset(72.0).build());
        parameters.add(new Parameter.Builder("TimingAdvance_3", "2101", new int[] {34}, "degree", "degree").multiplier(-0.375).offset(72.0).build());
        parameters.add(new Parameter.Builder("InjectorPulseWidth_0", "2101", new int[] {35, 36}, "мс", "msec").multiplier(0.004).wordLE(true).build());
        parameters.add(new Parameter.Builder("InjectorPulseWidth_1", "2101", new int[] {37, 38}, "мс", "msec").multiplier(0.004).wordLE(true).build());
        parameters.add(new Parameter.Builder("InjectorPulseWidth_2", "2101", new int[] {39, 40}, "мс", "msec").multiplier(0.004).wordLE(true).build());
        parameters.add(new Parameter.Builder("InjectorPulseWidth_3", "2101", new int[] {41, 42}, "мс", "msec").multiplier(0.004).wordLE(true).build());
        parameters.add(new Parameter.Builder("InjectorPulseWidth", "2101", new int[] {43, 44}, "мс", "msec").multiplier(0.004).wordLE(true).build());
        parameters.add(new Parameter.Builder("SOI", "2101", new int[] {45}, "degree", "degree").multiplier(-6.0).offset(180.0).build());
        parameters.add(new Parameter.Builder("STFT1", "2101", new int[] {48, 49}, "%", "%").multiplier(0.00152587890625).wordLE(true).build());
        parameters.add(new Parameter.Builder("LAM_MV_1", "2101", new int[] {50, 51}, "%", "%").multiplier(0.00152587890625).wordLE(true).build());
        parameters.add(new Parameter.Builder("TI_AD_ADD_MMV_1", "2101", new int[] {52, 53}, "ms", "ms").multiplier(0.004).wordLE(true).build());
        parameters.add(new Parameter.Builder("LTFT1", "2101", new int[] {54, 55}, "%", "%").multiplier(0.00152587890625).wordLE(true).build());
        parameters.add(new Parameter.Builder("FSMxx", "2101", new int[] {64}, "шаг", "step").build());
        parameters.add(new Parameter.Builder("TPS_SUB_DIAG", "2102", new int[] {2}, "%", "%").multiplier(0.4686).build());
        parameters.add(new Parameter.Builder("VLS_MMV_MAX_1", "2102", new int[] {56}, "В", "V").multiplier(7.62939453125E-5).build());
        parameters.add(new Parameter.Builder("VLS_MMV_MIN_1", "2102", new int[] {58}, "В", "V").multiplier(7.62939453125E-5).build());
        parameters.add(new Parameter.Builder("TCO_SUB", "2102", new int[] {72, 73}, "°С", "°С").multiplier(0.0029296875).offset(-48.0).wordLE(true).build());
        parameters.add(new Parameter.Builder("MAP_MDL_DIF", "2102", new int[] {78, 79}, "кПа", "kPa").multiplier(0.00829175).wordLE(true).build());
        siemens_kwp.super.setParameters(parameters);
    }
}
