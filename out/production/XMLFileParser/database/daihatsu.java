package tirex.ezway.libs.ecu;

import tirex.ezway.libs.Parameter;

import java.util.ArrayList;

public class daihatsu extends DefaultAuto {
    public daihatsu() {
        ArrayList<Parameter> parameters = new ArrayList<Parameter>();
        parameters.add(new Parameter.Builder("EngineLoad", "210401", new int[] {0}, "%", "%").multiplier(0.39216).build());
        parameters.add(new Parameter.Builder("STFT1", "210601", new int[] {0}, "%", "%").multiplier(0.78125).offset(-100.0).build());
        parameters.add(new Parameter.Builder("LTFT1", "210701", new int[] {0}, "%", "%").multiplier(0.78125).offset(-100.0).build());
        parameters.add(new Parameter.Builder("STFT2", "210801", new int[] {0}, "%", "%").multiplier(0.78125).offset(-100.0).build());
        parameters.add(new Parameter.Builder("LTFT2", "210901", new int[] {0}, "%", "%").multiplier(0.78125).offset(-100.0).build());
        parameters.add(new Parameter.Builder("FuelPressure", "210A01", new int[] {0}, "kpa", "kpa").build());
        parameters.add(new Parameter.Builder("TimingAdvance", "210E01", new int[] {0}, "degree", "degree").multiplier(0.5).build());
        parameters.add(new Parameter.Builder("ThrottlePosition", "211101", new int[] {0}, "%", "%").multiplier(0.390625).build());
        parameters.add(new Parameter.Builder("Oxygen_b1s1", "211401", new int[] {2}, "В", "V").multiplier(0.005).build());
        parameters.add(new Parameter.Builder("Oxygen_b1s1_stft", "211401", new int[] {3}, "%", "%").multiplier(0.7808).offset(-100.0).build());
        parameters.add(new Parameter.Builder("Oxygen_b1s2", "211501", new int[] {2}, "В", "V").multiplier(0.005).build());
        parameters.add(new Parameter.Builder("Oxygen_b1s2_stft", "211501", new int[] {3}, "%", "%").multiplier(0.7808).offset(-100.0).build());
        parameters.add(new Parameter.Builder("InjectorPulseWidth", "213401", new int[] {0}, "ms", "ms").multiplier(128.0).build());
        parameters.add(new Parameter.Builder("DistanceMIL", "212101", new int[] {0}, "км", "km").build());
        daihatsu.super.setParameters(parameters);
    }
}
