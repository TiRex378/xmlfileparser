package tirex.ezway.libs.ecu;

import tirex.ezway.libs.Parameter;

import java.util.ArrayList;

public class vaz_itelma_m73 extends DefaultAuto {
    public vaz_itelma_m73() {
        ArrayList<Parameter> parameters = new ArrayList<Parameter>();
        parameters.add(new Parameter.Builder("IntakeCoolantTemp", "2101", new int[] {3}, "°С", "°С").multiplier(0.75).offset(-48.0).build());
        parameters.add(new Parameter.Builder("CoolantTemp", "2101", new int[] {4}, "°С", "°С").multiplier(0.75).offset(-48.0).build());
        parameters.add(new Parameter.Builder("IntakeAirTemp", "2101", new int[] {5}, "°С", "°С").multiplier(0.75).offset(-48.0).build());
        parameters.add(new Parameter.Builder("ControlModuleVoltage", "2101", new int[] {6}, "В", "V").multiplier(0.073765).build());
        parameters.add(new Parameter.Builder("Speed", "2101", new int[] {7}, "kph", "kph").multiplier(1.25).build());
        parameters.add(new Parameter.Builder("ThrottlePosition", "2101", new int[] {8}, "%", "%").multiplier(0.392).build());
        parameters.add(new Parameter.Builder("RPM", "2101", new int[] {9}, "rpm", "rpm").multiplier(40.0).build());
        parameters.add(new Parameter.Builder("MAF", "2101", new int[] {10, 11}, "kgperhour", "kgperhour").multiplier(0.1).build());
        parameters.add(new Parameter.Builder("TimingAdvance", "2101", new int[] {12}, "degree", "degree").multiplier(0.75).build());
        parameters.add(new Parameter.Builder("DetUOZ", "2101", new int[] {13}, "degree", "degree").multiplier(0.75).build());
        parameters.add(new Parameter.Builder("EngineLoad", "2101", new int[] {14, 15}, "%", "%").multiplier(0.0234375).build());
        parameters.add(new Parameter.Builder("EngineLoadDesign", "2101", new int[] {16}, "%", "%").multiplier(0.75).build());
        parameters.add(new Parameter.Builder("InjectorPulseWidth", "2101", new int[] {18, 19}, "ms", "ms").multiplier(0.01332).build());
        parameters.add(new Parameter.Builder("JRPMSxx", "2101", new int[] {20}, "rpm", "rpm").multiplier(10.0).build());
        parameters.add(new Parameter.Builder("FSMxx", "2101", new int[] {21}, "шаг", "step").build());
        parameters.add(new Parameter.Builder("MAF_XX", "2101", new int[] {22, 23}, "kgperhour", "kgperhour").multiplier(0.001563).build());
        parameters.add(new Parameter.Builder("AdaptationXX", "2101", new int[] {24, 25}, "%", "%").multiplier(0.0030519).build());
        parameters.add(new Parameter.Builder("Oxygen_b1s1", "2101", new int[] {26}, "В", "V").multiplier(0.00521).offset(-0.2).build());
        parameters.add(new Parameter.Builder("CoefficientCanisterBleed", "2101", new int[] {31}, "%", "%").multiplier(0.390625).build());
        parameters.add(new Parameter.Builder("Detonation", "2101", new int[] {32, 33}, "В", "V").multiplier(0.0195).build());
        parameters.add(new Parameter.Builder("AvgCrankshaft", "2101", new int[] {34, 35}, "rpm/s^2", "rpm/s^2").multiplier(0.6).build());
        parameters.add(new Parameter.Builder("TimeMILCleared", "2101", new int[] {48, 49}, "h:min", "h:min").multiplier(0.1).build());
        parameters.add(new Parameter.Builder("LitersPerHour", "2101", new int[] {52, 53}, "lphour", "lphour").multiplier(0.00215).build());
        parameters.add(new Parameter.Builder("TRA", "2101", new int[] {61, 62}, "%", "%").multiplier(0.046875).build());
        parameters.add(new Parameter.Builder("FuelSystemStatus_Trim", "2101", new int[] {65}, "бит", "bit").build());
        parameters.add(new Parameter.Builder("ADC_SystemVoltage", "2103", new int[] {2, 3}, "В", "V").multiplier(0.0176).build());
        parameters.add(new Parameter.Builder("ADC_CoolantTemp", "2103", new int[] {4}, "В", "V").multiplier(0.019558824).build());
        parameters.add(new Parameter.Builder("ADC_MAF", "2103", new int[] {5, 6}, "В", "V").multiplier(0.004885).build());
        parameters.add(new Parameter.Builder("ADC_ThrottlePosition", "2103", new int[] {7, 8}, "В", "V").multiplier(7.6E-5).build());
        parameters.add(new Parameter.Builder("ADC_Oxygen_b1s1", "2103", new int[] {9, 10}, "В", "V").multiplier(0.004885).build());
        parameters.add(new Parameter.Builder("ADC_IntakeAirTemp", "2103", new int[] {11}, "В", "V").multiplier(0.01952).build());
        parameters.add(new Parameter.Builder("ADC_Oxygen_b1s1_OM", "2103", new int[] {14, 15}, "Om", "Om").multiplier(2.0).build());
        vaz_itelma_m73.super.setParameters(parameters);
    }
}
