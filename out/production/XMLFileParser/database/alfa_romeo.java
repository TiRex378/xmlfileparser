package tirex.ezway.libs.ecu;

import tirex.ezway.libs.Parameter;

import java.util.ArrayList;

public class alfa_romeo extends DefaultAuto {
    public alfa_romeo() {
        ArrayList<Parameter> parameters = new ArrayList<Parameter>();
        parameters.add(new Parameter.Builder("Speed", "2131", new int[] {1}, "kph", "kph").multiplier(0.007812).build());
        parameters.add(new Parameter.Builder("RPM", "2130", new int[] {1}, "rpm", "rpm").multiplier(0.25).build());
        alfa_romeo.super.setParameters(parameters);
    }
}
