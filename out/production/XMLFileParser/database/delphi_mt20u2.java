package tirex.ezway.libs.ecu;

import tirex.ezway.libs.Parameter;

import java.util.ArrayList;

public class delphi_mt20u2 extends DefaultAuto {
    public delphi_mt20u2() {
        ArrayList<Parameter> parameters = new ArrayList<Parameter>();
        parameters.add(new Parameter.Builder("Speed", "2101", new int[] {38}, "kph", "kph").build());
        parameters.add(new Parameter.Builder("MAF", "2101", new int[] {86}, "grpersec", "grpersec").multiplier(0.5).build());
        parameters.add(new Parameter.Builder("InjectorPulseWidth", "2101", new int[] {41, 42}, "мс", "msec").multiplier(0.0078125).build());
        parameters.add(new Parameter.Builder("CoolantTemp", "2101", new int[] {24}, "°С", "°С").multiplier(0.75).offset(-40.0).build());
        parameters.add(new Parameter.Builder("IntakeAirTemp", "2101", new int[] {22}, "°С", "°С").multiplier(0.75).offset(-40.0).build());
        parameters.add(new Parameter.Builder("RPM", "2101", new int[] {39, 40}, "rpm", "rpm").build());
        parameters.add(new Parameter.Builder("RPMIdle", "2101", new int[] {45}, "rpm", "rpm").multiplier(12.5).build());
        parameters.add(new Parameter.Builder("RunTime", "2101", new int[] {70, 71}, "секунда", "second").build());
        parameters.add(new Parameter.Builder("TimingAdvance", "2101", new int[] {25}, "°", "°").multiplier(0.703125).offset(-90.0).build());
        parameters.add(new Parameter.Builder("Oxygen_b1s1", "2101", new int[] {46}, "В", "V").multiplier(4.40625).build());
        parameters.add(new Parameter.Builder("ThrottlePosition", "2101", new int[] {37}, "%", "%").multiplier(0.3922).build());
        delphi_mt20u2.super.setParameters(parameters);
    }
}
