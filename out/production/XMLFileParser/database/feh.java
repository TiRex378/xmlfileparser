package tirex.ezway.libs.ecu;

import tirex.ezway.libs.Parameter;

import java.util.ArrayList;

public class feh extends DefaultAuto {
    public feh() {
        ArrayList<Parameter> parameters = new ArrayList<Parameter>();
        parameters.add(new Parameter.Builder("pBl", "224907", new int[] {1, 2}, "кОм", "kOhm").multiplier(0.0152588).build());
        parameters.add(new Parameter.Builder("nBl", "224908", new int[] {1, 2}, "кОм", "kOhm").multiplier(0.0152588).build());
        parameters.add(new Parameter.Builder("TBV", "22490B", new int[] {1, 2}, "В", "V").multiplier(0.03051758).build());
        parameters.add(new Parameter.Builder("Tmx", "224911", new int[] {3}, "°С", "°С").offset(-40.0).build());
        parameters.add(new Parameter.Builder("Tmn", "224911", new int[] {4}, "°С", "°С").offset(-40.0).build());
        parameters.add(new Parameter.Builder("Txc", "224911", new int[] {5}, "°С", "°С").offset(-40.0).build());
        parameters.add(new Parameter.Builder("Tav", "224911", new int[] {6}, "°С", "°С").offset(-40.0).build());
        parameters.add(new Parameter.Builder("TbC", "224915", new int[] {1, 2}, "A", "A").multiplier(0.030518).build());
        parameters.add(new Parameter.Builder("SoC", "224923", new int[] {1, 2}, "%", "%").multiplier(0.00152588).build());
        parameters.add(new Parameter.Builder("EvB", "22995A", new int[] {1}, "fahrenheit", "fahrenheit").multiplier(0.5).build());
        parameters.add(new Parameter.Builder("MxD", "22A90F", new int[] {1}, "kw", "kw").multiplier(0.25).build());
        parameters.add(new Parameter.Builder("MDV", "22A911", new int[] {1}, "В", "V").multiplier(0.0625).build());
        parameters.add(new Parameter.Builder("MxC", "22A912", new int[] {1}, "kw", "kw").multiplier(0.25).build());
        parameters.add(new Parameter.Builder("SoCmax", "22A913", new int[] {4, 5}, "%", "%").multiplier(0.00152588).build());
        parameters.add(new Parameter.Builder("SoCmin", "22A913", new int[] {6, 7}, "%", "%").multiplier(0.00152588).build());
        parameters.add(new Parameter.Builder("SoCavg", "22A913", new int[] {8, 9}, "%", "%").multiplier(0.00152588).build());
        parameters.add(new Parameter.Builder("SoCdelta", "22A913", new int[] {10, 11}, "%", "%").multiplier(0.00152588).build());
        parameters.add(new Parameter.Builder("EngineRPM", "22000C", new int[] {1, 2}, "rpm", "rpm").multiplier(0.25).build());
        parameters.add(new Parameter.Builder("BatVolt12", "221172", new int[] {1}, "В", "V").multiplier(0.0625).build());
        parameters.add(new Parameter.Builder("TxT", "221674", new int[] {1, 2}, "fahrenheit", "fahrenheit").multiplier(0.125).build());
        parameters.add(new Parameter.Builder("TracBatVol", "22490B", new int[] {1, 2}, "В", "V").multiplier(0.03051758).build());
        parameters.add(new Parameter.Builder("TrR", "22496A", new int[] {1, 2}, "rpm", "rpm").build());
        parameters.add(new Parameter.Builder("GenInvVolt", "22496C", new int[] {1, 2}, "В", "V").multiplier(0.1).build());
        parameters.add(new Parameter.Builder("MotInvVolt", "22496D", new int[] {1, 2}, "В", "V").multiplier(0.1).build());
        parameters.add(new Parameter.Builder("AmT", "22496E", new int[] {1, 2}, "A", "A").multiplier(0.1).build());
        parameters.add(new Parameter.Builder("MiT", "22496F", new int[] {1, 2}, "°С", "°С").build());
        parameters.add(new Parameter.Builder("Mco", "224970", new int[] {1, 2}, "°С", "°С").build());
        parameters.add(new Parameter.Builder("GiT", "224971", new int[] {1, 2}, "°С", "°С").build());
        parameters.add(new Parameter.Builder("Gco", "224972", new int[] {1, 2}, "°С", "°С").build());
        parameters.add(new Parameter.Builder("GnR", "224973", new int[] {1, 2}, "rpm", "rpm").build());
        parameters.add(new Parameter.Builder("TqD", "224974", new int[] {1, 2}, "Nm", "Nm").build());
        parameters.add(new Parameter.Builder("MotTorqDes", "224975", new int[] {1, 2}, "Nm", "Nm").build());
        parameters.add(new Parameter.Builder("GenTorqDes", "224976", new int[] {1, 2}, "Nm", "Nm").build());
        parameters.add(new Parameter.Builder("GTQ", "224977", new int[] {1, 2}, "Nm", "Nm").build());
        parameters.add(new Parameter.Builder("MTQ", "224978", new int[] {1, 2}, "Nm", "Nm").build());
        parameters.add(new Parameter.Builder("MeC", "224983", new int[] {1}, "°С", "°С").offset(-40.0).build());
        parameters.add(new Parameter.Builder("EngSpeedDes", "22A215", new int[] {1, 2}, "rpm", "rpm").build());
        feh.super.setParameters(parameters);
    }
}
