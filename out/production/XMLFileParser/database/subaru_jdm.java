package tirex.ezway.libs.ecu;

import tirex.ezway.libs.Parameter;

import java.util.ArrayList;

public class subaru_jdm extends DefaultAuto {
    public subaru_jdm() {
        ArrayList<Parameter> parameters = new ArrayList<Parameter>();
        parameters.add(new Parameter.Builder("EngineLoad", "A800000007", new int[] {4}, "%", "%").multiplier(0.390625).build());
        parameters.add(new Parameter.Builder("CoolantTemp", "A800000008", new int[] {4}, "°С", "°С").offset(-40.0).build());
        parameters.add(new Parameter.Builder("STFT1", "A800000009", new int[] {4}, "%", "%").multiplier(0.78125).offset(-100.0).build());
        parameters.add(new Parameter.Builder("LTFT1", "A80000000A", new int[] {4}, "%", "%").multiplier(0.78125).offset(-100.0).build());
        parameters.add(new Parameter.Builder("STFT2", "A80000000B", new int[] {4}, "%", "%").multiplier(0.78125).offset(-100.0).build());
        parameters.add(new Parameter.Builder("LTFT2", "A80000000C", new int[] {4}, "%", "%").multiplier(0.78125).offset(-100.0).build());
        parameters.add(new Parameter.Builder("IntakeManifoldPressure", "A80000000D", new int[] {4}, "кПа", "kPa").build());
        parameters.add(new Parameter.Builder("RPM", "A80000000E00000F", new int[] {4, 5}, "rpm", "rpm").offset(0.25).build());
        parameters.add(new Parameter.Builder("Speed", "A800000010", new int[] {4}, "kph", "kph").build());
        parameters.add(new Parameter.Builder("TimingAdvance", "A800000011", new int[] {4}, "degree", "degree").multiplier(0.5).offset(-64.0).build());
        parameters.add(new Parameter.Builder("IntakeAirTemp", "A800000012", new int[] {4}, "°С", "°С").offset(-40.0).build());
        parameters.add(new Parameter.Builder("MAF", "A800000013000014", new int[] {4, 5}, "grpersec", "grpersec").offset(0.01).build());
        parameters.add(new Parameter.Builder("ThrottlePosition", "A800000015", new int[] {4}, "%", "%").multiplier(0.390625).build());
        parameters.add(new Parameter.Builder("Oxygen_b1s1", "A800000016000017", new int[] {4, 5}, "В", "V").offset(0.005).build());
        parameters.add(new Parameter.Builder("Oxygen_b1s2", "A800000018000019", new int[] {4, 5}, "В", "V").offset(0.005).build());
        parameters.add(new Parameter.Builder("Oxygen_b2s1", "A80000001A00001B", new int[] {4, 5}, "В", "V").offset(0.005).build());
        parameters.add(new Parameter.Builder("ControlModuleVoltage", "A80000001C", new int[] {4}, "В", "V").multiplier(0.08).build());
        parameters.add(new Parameter.Builder("MAF_Voltage", "A80000001D", new int[] {4}, "В", "V").multiplier(0.02).build());
        parameters.add(new Parameter.Builder("ThrottlePositionVoltage", "A80000001E", new int[] {4}, "В", "V").multiplier(0.02).build());
        parameters.add(new Parameter.Builder("DifferentialPresSensorVoltage", "A80000001F", new int[] {4}, "В", "V").multiplier(0.02).build());
        parameters.add(new Parameter.Builder("InjectorPulseWidth", "A800000020", new int[] {4}, "ms", "ms").multiplier(0.256).build());
        parameters.add(new Parameter.Builder("InjectorPulseWidth2", "A800000021", new int[] {4}, "ms", "ms").multiplier(0.256).build());
        parameters.add(new Parameter.Builder("KnockCorrection", "A800000022", new int[] {4}, "degree", "degree").multiplier(0.5).offset(-64.0).build());
        parameters.add(new Parameter.Builder("BaroPressure", "A800000023", new int[] {4}, "кПа", "kPa").build());
        parameters.add(new Parameter.Builder("ManifoldRelativePressure", "A800000024", new int[] {4}, "кПа", "kPa").offset(-128.0).build());
        parameters.add(new Parameter.Builder("PressureDifferentialSensor", "A800000025", new int[] {4}, "кПа", "kPa").offset(-128.0).build());
        parameters.add(new Parameter.Builder("FuelTankPressure", "A800000026", new int[] {4}, "кПа", "kPa").multiplier(0.02413165048).offset(-3.0888512).build());
        parameters.add(new Parameter.Builder("CO_Adjustment", "A800000027", new int[] {4}, "В", "V").multiplier(0.02).build());
        parameters.add(new Parameter.Builder("LearnedIgnitionTiming", "A800000028", new int[] {4}, "degree", "degree").multiplier(0.5).offset(-64.0).build());
        parameters.add(new Parameter.Builder("PedalPosition", "A800000029", new int[] {4}, "%", "%").multiplier(0.390625).build());
        parameters.add(new Parameter.Builder("FuelTemp", "A80000002A", new int[] {4}, "°С", "°С").offset(-40.0).build());
        parameters.add(new Parameter.Builder("Oxygen_b1s1_Heater", "A80000002B", new int[] {4}, "A", "A").multiplier(0.03921875).build());
        parameters.add(new Parameter.Builder("Oxygen_b1s2_Heater", "A80000002С", new int[] {4}, "A", "A").multiplier(0.03921875).build());
        parameters.add(new Parameter.Builder("Oxygen_b2s1_Heater", "A80000002D", new int[] {4}, "A", "A").multiplier(0.03921875).build());
        parameters.add(new Parameter.Builder("FuelLevel", "A80000002E", new int[] {4}, "В", "V").multiplier(0.02).build());
        subaru_jdm.super.setParameters(parameters);
    }
}
