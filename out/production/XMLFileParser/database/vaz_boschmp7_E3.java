package tirex.ezway.libs.ecu;

import tirex.ezway.libs.Parameter;

import java.util.ArrayList;

public class vaz_boschmp7_E3 extends DefaultAuto {
    public vaz_boschmp7_E3() {
        ArrayList<Parameter> parameters = new ArrayList<Parameter>();
        parameters.add(new Parameter.Builder("FuelSystemStatus_Trim", "Core", new int[] {84}, "бит", "bit").build());
        parameters.add(new Parameter.Builder("ECUTime", "Core", new int[] {41, 42}, "час", "hour").multiplier(0.1).build());
        parameters.add(new Parameter.Builder("ParameterStorageTime", "Core", new int[] {43, 44}, "degree", "degree").multiplier(0.74609375).build());
        parameters.add(new Parameter.Builder("IntakeAirTemp", "Core", new int[] {45}, "°С", "°С").multiplier(0.75).offset(-48.0).build());
        parameters.add(new Parameter.Builder("IntakeCoolantTemp", "Core", new int[] {46}, "°С", "°С").multiplier(0.75).offset(-48.0).build());
        parameters.add(new Parameter.Builder("RoughRoad", "Core", new int[] {47}, "g", "g").multiplier(0.02451171875).offset(-6.275).build());
        parameters.add(new Parameter.Builder("EngineLoadDesign", "Core", new int[] {48}, "ms", "ms").multiplier(0.05).build());
        vaz_boschmp7_E3.super.setParameters(parameters);
    }
}
