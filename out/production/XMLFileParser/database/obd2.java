package tirex.ezway.libs.ecu;

import tirex.ezway.libs.Parameter;

import java.util.ArrayList;

public class obd2 extends DefaultAuto {
    public obd2() {
        ArrayList<Parameter> parameters = new ArrayList<Parameter>();
        parameters.add(new Parameter.Builder("ATFTemp", "2102", new int[] {6}, "°С", "°С").offset(-40.0).build());
        obd2.super.setParameters(parameters);
    }
}
