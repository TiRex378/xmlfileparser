package tirex.ezway.libs.ecu;

import tirex.ezway.libs.Parameter;

import java.util.ArrayList;

public class delphi_mt20u extends DefaultAuto {
    public delphi_mt20u() {
        ArrayList<Parameter> parameters = new ArrayList<Parameter>();
        parameters.add(new Parameter.Builder("Speed", "2101", new int[] {25}, "kph", "kph").build());
        parameters.add(new Parameter.Builder("MAF", "2101", new int[] {73}, "grpersec", "grpersec").multiplier(0.5).build());
        parameters.add(new Parameter.Builder("InjectorPulseWidth", "2101", new int[] {28, 29}, "мс", "msec").multiplier(0.0078125).build());
        parameters.add(new Parameter.Builder("CoolantTemp", "2101", new int[] {14}, "°С", "°С").multiplier(0.75).offset(-40.0).build());
        parameters.add(new Parameter.Builder("IntakeAirTemp", "2101", new int[] {12}, "°С", "°С").multiplier(0.75).offset(-40.0).build());
        parameters.add(new Parameter.Builder("RPM", "2101", new int[] {26, 27}, "rpm", "rpm").build());
        parameters.add(new Parameter.Builder("RunTime", "2101", new int[] {57, 58}, "секунда", "second").build());
        parameters.add(new Parameter.Builder("FuelEconomy_trip", "2101", new int[] {76, 77}, "литр", "liter").multiplier(0.002).build());
        parameters.add(new Parameter.Builder("TimingAdvance", "2101", new int[] {15}, "°", "°").multiplier(0.703125).offset(-90.0).build());
        parameters.add(new Parameter.Builder("ThrottlePosition", "2101", new int[] {24}, "%", "%").multiplier(0.3921).build());
        parameters.add(new Parameter.Builder("Oxygen_b1s1", "2101", new int[] {33}, "В", "V").multiplier(0.0044).build());
        parameters.add(new Parameter.Builder("Oxygen_b1s2", "2101", new int[] {69}, "В", "V").multiplier(0.00434).build());
        delphi_mt20u.super.setParameters(parameters);
    }
}
