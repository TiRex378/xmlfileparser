package tirex.ezway.libs.ecu;

import tirex.ezway.libs.Parameter;

import java.util.ArrayList;

public class opel_VB_X16XEL extends DefaultAuto {
    public opel_VB_X16XEL() {
        ArrayList<Parameter> parameters = new ArrayList<Parameter>();
        parameters.add(new Parameter.Builder("ControlModuleVoltage", "2101", new int[] {10}, "В", "V").multiplier(0.1).build());
        parameters.add(new Parameter.Builder("IntakeAirTempVoltage", "2101", new int[] {11}, "В", "V").multiplier(0.01953125).build());
        parameters.add(new Parameter.Builder("IntakeAirTemp", "2101", new int[] {12}, "°С", "°С").multiplier(0.75).offset(-40.0).build());
        parameters.add(new Parameter.Builder("CoolantTempVoltage", "2101", new int[] {13}, "В", "V").multiplier(0.01953125).build());
        parameters.add(new Parameter.Builder("CoolantTemp", "2101", new int[] {14}, "°С", "°С").multiplier(0.75).offset(-40.0).build());
        parameters.add(new Parameter.Builder("EngineLoad", "2101", new int[] {16}, "%", "%").multiplier(0.390625).build());
        parameters.add(new Parameter.Builder("TimingAdvance", "2101", new int[] {15}, "degree", "degree").multiplier(0.703125).offset(-90.0).build());
        parameters.add(new Parameter.Builder("DetUOZ_1", "2101", new int[] {19}, "degree", "degree").multiplier(0.3515625).build());
        parameters.add(new Parameter.Builder("DetUOZ_2", "2101", new int[] {20}, "degree", "degree").multiplier(0.3515625).build());
        parameters.add(new Parameter.Builder("DetUOZ_3", "2101", new int[] {21}, "degree", "degree").multiplier(0.3515625).build());
        parameters.add(new Parameter.Builder("DetUOZ_4", "2101", new int[] {22}, "degree", "degree").multiplier(0.3515625).build());
        parameters.add(new Parameter.Builder("ThrottlePositionVoltage", "2101", new int[] {23}, "В", "V").multiplier(0.01953125).build());
        parameters.add(new Parameter.Builder("ThrottlePosition", "2101", new int[] {24}, "%", "%").multiplier(0.390625).build());
        parameters.add(new Parameter.Builder("Speed", "2101", new int[] {25}, "kph", "kph").build());
        parameters.add(new Parameter.Builder("RPM", "2101", new int[] {26}, "rpm", "rpm").multiplier(25.0).build());
        parameters.add(new Parameter.Builder("InjectorPulseWidth", "2101", new int[] {27}, "ms", "ms").multiplier(0.08515625).build());
        parameters.add(new Parameter.Builder("Oxygen_b1s1", "2101", new int[] {31}, "В", "V").multiplier(0.00442).build());
        parameters.add(new Parameter.Builder("FSMxx", "2101", new int[] {32}, "шаг", "step").build());
        opel_VB_X16XEL.super.setParameters(parameters);
    }
}
