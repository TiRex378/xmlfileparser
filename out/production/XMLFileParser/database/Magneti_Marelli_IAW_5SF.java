package tirex.ezway.libs.ecu;

import tirex.ezway.libs.Parameter;

import java.util.ArrayList;

public class Magneti_Marelli_IAW_5SF extends DefaultAuto {
    public Magneti_Marelli_IAW_5SF() {
        ArrayList<Parameter> parameters = new ArrayList<Parameter>();
        parameters.add(new Parameter.Builder("RPM", "2130", new int[] {0, 1}, "rpm", "rpm").build());
        parameters.add(new Parameter.Builder("IntakeManifoldPressure", "2131", new int[] {0, 1}, "Millibar", "Millibar").build());
        parameters.add(new Parameter.Builder("IntakeAirTemp", "2132", new int[] {0, 1}, "°С", "°С").offset(-40.0).build());
        parameters.add(new Parameter.Builder("CoolantTemp", "2133", new int[] {0, 1}, "°С", "°С").offset(-40.0).build());
        parameters.add(new Parameter.Builder("ThrottlePosition1", "2134", new int[] {0, 1}, "degree", "degree").multiplier(0.1).build());
        parameters.add(new Parameter.Builder("2135", "2135", new int[] {0, 1}, "degree", "degree").multiplier(0.1).build());
        parameters.add(new Parameter.Builder("TimingAdvance", "2137", new int[] {0, 1}, "degree", "degree").multiplier(0.1).build());
        parameters.add(new Parameter.Builder("EngineLoad", "2138", new int[] {0, 1}, "kgperhour", "kgperhour").multiplier(0.125).build());
        parameters.add(new Parameter.Builder("InjectorPulseWidth", "2139", new int[] {0, 1}, "ms", "ms").multiplier(0.002).build());
        parameters.add(new Parameter.Builder("213a", "213a", new int[] {0, 1}, "rpm", "rpm").build());
        parameters.add(new Parameter.Builder("213b", "213b", new int[] {0, 1}, "ms", "ms").multiplier(0.002).build());
        parameters.add(new Parameter.Builder("2141", "2141", new int[] {0, 1}, "mВ", "mV").multiplier(4.44).build());
        parameters.add(new Parameter.Builder("2142", "2142", new int[] {0, 1}, "mВ", "mV").multiplier(4.44).build());
        parameters.add(new Parameter.Builder("2143", "2143", new int[] {0, 1}, "mВ", "mV").multiplier(4.44).build());
        parameters.add(new Parameter.Builder("2144", "2144", new int[] {0, 1}, "mВ", "mV").multiplier(4.44).build());
        parameters.add(new Parameter.Builder("2145", "2145", new int[] {0, 1}, "mВ", "mV").build());
        parameters.add(new Parameter.Builder("2147", "2147", new int[] {0, 1}, "%", "%").multiplier(2.5E-5).build());
        parameters.add(new Parameter.Builder("2148", "2148", new int[] {0, 1}, "%", "%").multiplier(2.5E-5).build());
        parameters.add(new Parameter.Builder("Speed", "2153", new int[] {0, 1}, "kph", "kph").build());
        parameters.add(new Parameter.Builder("2154", "2154", new int[] {0, 1}, "ms", "ms").multiplier(0.002).build());
        parameters.add(new Parameter.Builder("2155", "2155", new int[] {0, 1}, "ms", "ms").multiplier(0.002).build());
        parameters.add(new Parameter.Builder("2156", "2156", new int[] {0, 1}, "ms", "ms").build());
        parameters.add(new Parameter.Builder("215c", "215c", new int[] {0, 1}, "%", "%").multiplier(0.1).build());
        parameters.add(new Parameter.Builder("215d", "215d", new int[] {0, 1}, "%", "%").multiplier(2.5E-5).build());
        parameters.add(new Parameter.Builder("2169", "2169", new int[] {0, 1}, "mВ", "mV").multiplier(4.88).build());
        parameters.add(new Parameter.Builder("216a", "216a", new int[] {0, 1}, "mВ", "mV").multiplier(4.88).build());
        parameters.add(new Parameter.Builder("216f", "216f", new int[] {0, 1}, "ms", "ms").multiplier(0.002).build());
        parameters.add(new Parameter.Builder("2173", "2173", new int[] {0, 1}, "%", "%").multiplier(0.1).build());
        parameters.add(new Parameter.Builder("2178", "2178", new int[] {0, 1}, "degree", "degree").multiplier(0.1).build());
        parameters.add(new Parameter.Builder("EngineOilTemp", "21d9", new int[] {0, 1}, "degree", "degree").build());
        parameters.add(new Parameter.Builder("21da", "21da", new int[] {0, 1}, "Nm", "Nm").multiplier(0.1).build());
        parameters.add(new Parameter.Builder("21db", "21db", new int[] {0, 1}, "Nm", "Nm").multiplier(0.1).build());
        parameters.add(new Parameter.Builder("21dc", "21dc", new int[] {0, 1}, "Nm", "Nm").multiplier(0.1).build());
        parameters.add(new Parameter.Builder("21dd", "21dd", new int[] {0, 1}, "Nm", "Nm").multiplier(0.1).build());
        parameters.add(new Parameter.Builder("21de", "21de", new int[] {0, 1}, "Nm", "Nm").multiplier(0.1).build());
        parameters.add(new Parameter.Builder("LitersPerHour", "21df", new int[] {0, 1}, "lphour", "lphour").multiplier(0.00215).build());
        Magneti_Marelli_IAW_5SF.super.setParameters(parameters);
    }
}
