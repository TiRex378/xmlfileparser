package tirex.ezway.libs.ecu;

import tirex.ezway.libs.Parameter;

import java.util.ArrayList;

public class vaz_m73_797_E3 extends DefaultAuto {
    public vaz_m73_797_E3() {
        ArrayList<Parameter> parameters = new ArrayList<Parameter>();
        parameters.add(new Parameter.Builder("RPMXX", "Core", new int[] {67}, "rpm", "rpm").multiplier(10.0).build());
        parameters.add(new Parameter.Builder("Oxygen_b1s2", "Core", new int[] {68}, "В", "V").multiplier(0.00521).offset(-0.2).build());
        parameters.add(new Parameter.Builder("Oxygen_b1s1_period", "Core", new int[] {69}, "секунда", "second").multiplier(0.004).build());
        parameters.add(new Parameter.Builder("Oxygen_b1s2_break", "Core", new int[] {70}, "секунда", "second").multiplier(0.01).build());
        parameters.add(new Parameter.Builder("Oxygen_b1s2_amplitudeAdjust", "Core", new int[] {71}, "секунда", "second").multiplier(0.00390625).build());
        parameters.add(new Parameter.Builder("RoughRoad", "Core", new int[] {72, 73}, "g", "g").multiplier(1.9073486328125E-4).offset(-6.25).build());
        parameters.add(new Parameter.Builder("ADC_Oxygen_b1s2", "Core2103", new int[] {12, 13}, "В", "V").multiplier(0.004885).build());
        parameters.add(new Parameter.Builder("ADC_Oxygen_b1s2_OM", "Core2103", new int[] {19, 20}, "Om", "Om").multiplier(2.0).build());
        vaz_m73_797_E3.super.setParameters(parameters);
    }
}
