package tirex.ezway.libs.ecu;

import tirex.ezway.libs.Parameter;

import java.util.ArrayList;

public class bosch_me1797 extends DefaultAuto {
    public bosch_me1797() {
        ArrayList<Parameter> parameters = new ArrayList<Parameter>();
        parameters.add(new Parameter.Builder("ECUtime", "2104", new int[] {6, 7}, "минута", "minute").multiplier(6.0).build());
        parameters.add(new Parameter.Builder("RPM", "2101", new int[] {2, 3}, "rpm", "rpm").multiplier(0.25).build());
        parameters.add(new Parameter.Builder("RPM_NSOL", "2101", new int[] {4, 5}, "rpm", "rpm").multiplier(0.25).build());
        parameters.add(new Parameter.Builder("CoolantTemp", "2101", new int[] {9}, "°С", "°С").multiplier(0.75).offset(-48.0).build());
        parameters.add(new Parameter.Builder("TMST", "2101", new int[] {10}, "°С", "°С").multiplier(0.75).offset(-48.0).build());
        parameters.add(new Parameter.Builder("IntakeAirTemp", "2101", new int[] {11}, "°С", "°С").multiplier(0.75).offset(-48.0).build());
        parameters.add(new Parameter.Builder("TimingAdvance", "2101", new int[] {13}, "°", "°").multiplier(0.75).build());
        parameters.add(new Parameter.Builder("UOZ_1", "2101", new int[] {13}, "°", "°").multiplier(0.75).build());
        parameters.add(new Parameter.Builder("UOZ_3", "2101", new int[] {14}, "°", "°").multiplier(0.75).build());
        parameters.add(new Parameter.Builder("UOZ_4", "2101", new int[] {15}, "°", "°").multiplier(0.75).build());
        parameters.add(new Parameter.Builder("UOZ_2", "2101", new int[] {16}, "°", "°").multiplier(0.75).build());
        parameters.add(new Parameter.Builder("MAF", "2101", new int[] {19, 20}, "kgperhour", "kgperhour").multiplier(0.1).build());
        parameters.add(new Parameter.Builder("PedalPosition", "2101", new int[] {23, 24}, "%", "%").multiplier(0.0015259).build());
        parameters.add(new Parameter.Builder("ThrottlePosition", "2101", new int[] {25, 26}, "%", "%").multiplier(0.02441406).build());
        parameters.add(new Parameter.Builder("InjectorPulseWidth", "2101", new int[] {27, 28}, "ms", "ms").multiplier(0.0064).build());
        parameters.add(new Parameter.Builder("EngineLoad", "2101", new int[] {29, 30}, "%", "%").multiplier(0.0234375).build());
        parameters.add(new Parameter.Builder("DMVAD", "2101", new int[] {35, 36}, "%", "%").multiplier(0.00305176).build());
        parameters.add(new Parameter.Builder("ControlModuleVoltage", "2101", new int[] {49}, "v", "v").multiplier(0.1).build());
        parameters.add(new Parameter.Builder("Oxygen_b1s1", "2101", new int[] {60}, "v", "v").multiplier(0.005).build());
        parameters.add(new Parameter.Builder("Oxygen_b1s2", "2101", new int[] {61}, "v", "v").multiplier(0.005).build());
        parameters.add(new Parameter.Builder("DetUOZ_1", "2101", new int[] {86}, "°", "°").multiplier(0.75).build());
        parameters.add(new Parameter.Builder("DetUOZ_3", "2101", new int[] {87}, "°", "°").multiplier(0.75).build());
        parameters.add(new Parameter.Builder("DetUOZ_4", "2101", new int[] {88}, "°", "°").multiplier(0.75).build());
        parameters.add(new Parameter.Builder("DetUOZ_2", "2101", new int[] {89}, "°", "°").multiplier(0.75).build());
        bosch_me1797.super.setParameters(parameters);
    }
}
