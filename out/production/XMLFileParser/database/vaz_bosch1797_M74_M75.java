package tirex.ezway.libs.ecu;

import tirex.ezway.libs.Parameter;

import java.util.ArrayList;

public class vaz_bosch1797_M74_M75 extends DefaultAuto {
    public vaz_bosch1797_M74_M75() {
        ArrayList<Parameter> parameters = new ArrayList<Parameter>();
        parameters.add(new Parameter.Builder("ControlModuleVoltage", "2101", new int[] {3}, "В", "V").multiplier(0.1).build());
        parameters.add(new Parameter.Builder("CoolantTemp", "2101", new int[] {4, 5}, "°С", "°С").multiplier(0.75).offset(-48.0).build());
        parameters.add(new Parameter.Builder("RPM", "2101", new int[] {6, 7}, "rpm", "rpm").multiplier(0.25).build());
        parameters.add(new Parameter.Builder("Speed", "2101", new int[] {8}, "kph", "kph").multiplier(1.25).build());
        parameters.add(new Parameter.Builder("TimingAdvance", "2101", new int[] {9}, "degree", "degree").multiplier(0.75).build());
        parameters.add(new Parameter.Builder("InjectorPulseWidth", "2101", new int[] {10, 11}, "ms", "ms").multiplier(0.00634).build());
        parameters.add(new Parameter.Builder("ThrottlePosition", "2101", new int[] {12, 13}, "%", "%").multiplier(0.02441406).build());
        parameters.add(new Parameter.Builder("PedalPosition", "2101", new int[] {14, 15}, "%", "%").multiplier(0.0015259).build());
        parameters.add(new Parameter.Builder("EngineLoad", "2101", new int[] {16, 17}, "%", "%").multiplier(0.0234375).build());
        parameters.add(new Parameter.Builder("MAF", "2101", new int[] {18, 19}, "kgperhour", "kgperhour").multiplier(0.1).build());
        parameters.add(new Parameter.Builder("Oxygen_b1s1", "2101", new int[] {21}, "В", "V").multiplier(0.00502).build());
        parameters.add(new Parameter.Builder("Oxygen_b1s1_period", "2101", new int[] {22}, "ms", "ms").multiplier(0.00502).build());
        parameters.add(new Parameter.Builder("Oxygen_b1s2", "2101", new int[] {23}, "В", "V").multiplier(0.00502).build());
        parameters.add(new Parameter.Builder("AltitudeCorrection", "2101", new int[] {27, 28}, "кг", "kg").multiplier(4.0E-4).build());
        parameters.add(new Parameter.Builder("LTFT1", "2101", new int[] {29, 30}, "%", "%").multiplier(3.05176E-5).build());
        parameters.add(new Parameter.Builder("LitersPerHour", "2101", new int[] {33, 34}, "lphour", "lphour").multiplier(0.00215).build());
        parameters.add(new Parameter.Builder("IntakeAirTemp", "2101", new int[] {35}, "°С", "°С").multiplier(0.75).offset(-48.0).build());
        parameters.add(new Parameter.Builder("CoefficientCanisterBleed", "2101", new int[] {36, 37}, "%", "%").multiplier(0.001526).build());
        parameters.add(new Parameter.Builder("DetUOZ", "2101", new int[] {40}, "degree", "degree").multiplier(0.75).build());
        parameters.add(new Parameter.Builder("JRPMSxx", "2101", new int[] {41}, "rpm", "rpm").multiplier(10.0).build());
        parameters.add(new Parameter.Builder("DifTorqueAdapted", "2101", new int[] {42, 43}, "%", "%").multiplier(0.0030519).build());
        parameters.add(new Parameter.Builder("AirLeakageThrottle", "2101", new int[] {44, 45}, "kgperhour", "kgperhour").multiplier(0.1).build());
        parameters.add(new Parameter.Builder("RegulationXXI", "2101", new int[] {46, 47}, "%", "%").multiplier(0.0030519).build());
        parameters.add(new Parameter.Builder("RegulationXXPD", "2101", new int[] {48, 49}, "%", "%").multiplier(0.0030519).build());
        parameters.add(new Parameter.Builder("IntakeCoolantTemp", "2101", new int[] {50}, "°С", "°С").multiplier(0.75).offset(-48.0).build());
        parameters.add(new Parameter.Builder("FuelSystemStatus_Trim", "2101", new int[] {55}, "бит", "bit").build());
        parameters.add(new Parameter.Builder("DetonationStatus", "2101", new int[] {55}, "бит", "bit").build());
        vaz_bosch1797_M74_M75.super.setParameters(parameters);
    }
}
