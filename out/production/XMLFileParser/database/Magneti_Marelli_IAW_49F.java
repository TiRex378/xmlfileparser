package tirex.ezway.libs.ecu;

import tirex.ezway.libs.Parameter;

import java.util.ArrayList;

public class Magneti_Marelli_IAW_49F extends DefaultAuto {
    public Magneti_Marelli_IAW_49F() {
        ArrayList<Parameter> parameters = new ArrayList<Parameter>();
        parameters.add(new Parameter.Builder("RPM", "2130", new int[] {0, 1}, "rpm", "rpm").build());
        parameters.add(new Parameter.Builder("IntakeManifoldPressure", "2131", new int[] {0, 1}, "Millibar", "Millibar").build());
        parameters.add(new Parameter.Builder("IntakeAirTemp", "2132", new int[] {0, 1}, "°С", "°С").offset(-40.0).build());
        parameters.add(new Parameter.Builder("CoolantTemp", "2133", new int[] {0, 1}, "°С", "°С").offset(-40.0).build());
        parameters.add(new Parameter.Builder("2134", "2134", new int[] {0, 1}, "degree", "degree").multiplier(0.1).build());
        parameters.add(new Parameter.Builder("2135", "2135", new int[] {0, 1}, "degree", "degree").offset(0.1).build());
        parameters.add(new Parameter.Builder("2136", "2136", new int[] {0, 1}, "degree", "degree").build());
        parameters.add(new Parameter.Builder("TimingAdvance", "2137", new int[] {0, 1}, "degree", "degree").offset(0.1).build());
        parameters.add(new Parameter.Builder("InjectorPulseWidth", "2138", new int[] {0, 1}, "ms", "ms").multiplier(0.002).build());
        parameters.add(new Parameter.Builder("EngineLoad", "2139", new int[] {0, 1}, "kgperhour", "kgperhour").multiplier(0.125).build());
        parameters.add(new Parameter.Builder("213A", "213A", new int[] {0, 1}, "ms", "ms").multiplier(0.002).build());
        parameters.add(new Parameter.Builder("213B", "213B", new int[] {0, 1}, "ms", "ms").multiplier(0.002).build());
        parameters.add(new Parameter.Builder("213C", "213C", new int[] {0, 1}, "ms", "ms").multiplier(0.002).build());
        parameters.add(new Parameter.Builder("213D", "213D", new int[] {0, 1}, "ms", "ms").multiplier(0.002).build());
        parameters.add(new Parameter.Builder("RPMSetup", "213E", new int[] {0, 1}, "rpm", "rpm").build());
        parameters.add(new Parameter.Builder("FSMxx", "2140", new int[] {0, 1}, "шаг", "step").build());
        parameters.add(new Parameter.Builder("ControlModuleVoltage", "2141", new int[] {0, 1}, "В", "V").multiplier(0.1).build());
        parameters.add(new Parameter.Builder("2142", "2142", new int[] {0, 1}, "degree", "degree").multiplier(0.1).build());
        parameters.add(new Parameter.Builder("2143", "2143", new int[] {0, 1}, "degree", "degree").multiplier(0.1).build());
        parameters.add(new Parameter.Builder("2144", "2144", new int[] {0, 1}, "degree", "degree").multiplier(0.1).build());
        parameters.add(new Parameter.Builder("2145", "2145", new int[] {0, 1}, "degree", "degree").multiplier(0.1).build());
        parameters.add(new Parameter.Builder("2146", "2146", new int[] {0, 1}, "mВ", "mV").multiplier(4.44).build());
        parameters.add(new Parameter.Builder("2147", "2147", new int[] {0, 1}, "mВ", "mV").multiplier(4.44).build());
        parameters.add(new Parameter.Builder("2148", "2148", new int[] {0, 1}, "mВ", "mV").multiplier(4.44).build());
        parameters.add(new Parameter.Builder("2149", "2149", new int[] {0, 1}, "mВ", "mV").multiplier(4.44).build());
        parameters.add(new Parameter.Builder("214C", "214C", new int[] {0, 1}, "mВ", "mV").build());
        parameters.add(new Parameter.Builder("2150", "2150", new int[] {0, 1}, "%", "%").multiplier(2.5E-5).build());
        parameters.add(new Parameter.Builder("Speed", "215A", new int[] {0, 1}, "kph", "kph").build());
        parameters.add(new Parameter.Builder("215D", "215D", new int[] {0, 1}, "ms", "ms").build());
        Magneti_Marelli_IAW_49F.super.setParameters(parameters);
    }
}
