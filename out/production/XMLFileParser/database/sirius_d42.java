package tirex.ezway.libs.ecu;

import tirex.ezway.libs.Parameter;

import java.util.ArrayList;

public class sirius_d42 extends DefaultAuto {
    public sirius_d42() {
        ArrayList<Parameter> parameters = new ArrayList<Parameter>();
        parameters.add(new Parameter.Builder("CoolantTemp", "2102", new int[] {2}, "°С", "°С").multiplier(0.75).offset(-48.75).build());
        parameters.add(new Parameter.Builder("AirTemp", "2102", new int[] {3}, "°С", "°С").multiplier(0.75).offset(-48.75).build());
        parameters.add(new Parameter.Builder("ThrottlePosition", "2102", new int[] {4}, "%", "%").multiplier(0.392).build());
        parameters.add(new Parameter.Builder("ControlModuleVoltage", "2102", new int[] {5}, "В", "V").multiplier(0.11111).offset(1.66666).build());
        parameters.add(new Parameter.Builder("Speed", "2102", new int[] {6}, "kph", "kph").build());
        parameters.add(new Parameter.Builder("RPM", "2102", new int[] {7, 8}, "rpm", "rpm").build());
        parameters.add(new Parameter.Builder("BaroPressure", "2102", new int[] {9}, "kpa", "kpa").multiplier(0.465).build());
        parameters.add(new Parameter.Builder("IntakeManifoldPressure", "2102", new int[] {11}, "kpa", "kpa").multiplier(0.465).build());
        parameters.add(new Parameter.Builder("MAPStep1", "2102", new int[] {13}, "mg", "mg").multiplier(5.421).build());
        parameters.add(new Parameter.Builder("MAPStep2", "2102", new int[] {14}, "mg", "mg").multiplier(5.421).build());
        parameters.add(new Parameter.Builder("InjectorPulseWidthStart", "2102", new int[] {15, 16}, "ms", "ms").multiplier(0.016).build());
        parameters.add(new Parameter.Builder("InjectorPulseWidth", "2102", new int[] {17, 18}, "ms", "ms").multiplier(0.004).build());
        parameters.add(new Parameter.Builder("InjectorPulseWidthCorrection", "2102", new int[] {21, 22}, "ms", "ms").build());
        parameters.add(new Parameter.Builder("AdsorberDutyCycle", "2102", new int[] {25}, "%", "%").multiplier(0.39).build());
        parameters.add(new Parameter.Builder("ExhausLinearity", "2102", new int[] {26}, "%", "%").multiplier(0.39).build());
        parameters.add(new Parameter.Builder("ECUFuelLevel", "2102", new int[] {27}, "%", "%").multiplier(0.392).build());
        parameters.add(new Parameter.Builder("ACPressure", "2102", new int[] {29}, "kpa", "kpa").multiplier(12.15).build());
        sirius_d42.super.setParameters(parameters);
    }
}
