package tirex.ezway.libs.ecu;

import tirex.ezway.libs.Parameter;

import java.util.ArrayList;

public class Hyundai_G4EC extends DefaultAuto {
    public Hyundai_G4EC() {
        ArrayList<Parameter> parameters = new ArrayList<Parameter>();
        parameters.add(new Parameter.Builder("IntakeManifoldPressure", "2102", new int[] {8, 9}, "kpa", "kpa").multiplier(0.1).wordLE(true).build());
        parameters.add(new Parameter.Builder("ControlModuleVoltage", "2103", new int[] {4}, "В", "V").multiplier(0.07).build());
        parameters.add(new Parameter.Builder("CoolantTemp", "2103", new int[] {5}, "°С", "°С").multiplier(0.75).offset(-48.0).build());
        parameters.add(new Parameter.Builder("IntakeCoolantTemp", "2103", new int[] {6}, "°С", "°С").multiplier(0.75).offset(-48.0).build());
        parameters.add(new Parameter.Builder("ThrottlePosition", "2103", new int[] {7}, "%", "%").multiplier(0.5).build());
        parameters.add(new Parameter.Builder("InjectorPulseWidth1", "2104", new int[] {6, 7}, "ms", "ms").multiplier(0.003395).wordLE(true).build());
        parameters.add(new Parameter.Builder("InjectorPulseWidth2", "2104", new int[] {8, 9}, "ms", "ms").multiplier(0.003395).wordLE(true).build());
        parameters.add(new Parameter.Builder("InjectorPulseWidth3", "2104", new int[] {10, 11}, "ms", "ms").multiplier(0.003395).wordLE(true).build());
        parameters.add(new Parameter.Builder("InjectorPulseWidth4", "2104", new int[] {12, 13}, "ms", "ms").multiplier(0.003395).wordLE(true).build());
        parameters.add(new Parameter.Builder("InjectorPulseWidth", "2104", new int[] {14, 15}, "ms", "ms").multiplier(2.44140625E-4).wordLE(true).build());
        parameters.add(new Parameter.Builder("Oxygen_b1s1", "2105", new int[] {4}, "В", "V").multiplier(0.005).offset(-0.1).build());
        parameters.add(new Parameter.Builder("Oxygen_b1s2", "2105", new int[] {5}, "В", "V").multiplier(0.005).offset(-0.1).build());
        parameters.add(new Parameter.Builder("Speed", "2106", new int[] {2}, "kph", "kph").build());
        parameters.add(new Parameter.Builder("RPM", "2106", new int[] {5, 6}, "rpm", "rpm").multiplier(0.25).wordLE(true).build());
        parameters.add(new Parameter.Builder("LTFT1", "2106", new int[] {10, 11}, "%", "%").multiplier(0.0030517578125).offset(-100.0).wordLE(true).build());
        Hyundai_G4EC.super.setParameters(parameters);
    }
}
