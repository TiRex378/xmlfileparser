package tirex.ezway.libs.ecu;

import tirex.ezway.libs.Parameter;

import java.util.ArrayList;

public class opel_VB_X18XE extends DefaultAuto {
    public opel_VB_X18XE() {
        ArrayList<Parameter> parameters = new ArrayList<Parameter>();
        parameters.add(new Parameter.Builder("ControlModuleVoltage", "2101", new int[] {11}, "В", "V").multiplier(0.1).build());
        parameters.add(new Parameter.Builder("IntakeAirTempVoltage", "2101", new int[] {12}, "В", "V").multiplier(0.01953125).build());
        parameters.add(new Parameter.Builder("IntakeAirTemp", "2101", new int[] {12}, "°С", "°С").multiplier(-0.4).offset(62.0).build());
        parameters.add(new Parameter.Builder("CoolantTempVoltage", "2101", new int[] {13}, "В", "V").multiplier(0.01953125).build());
        parameters.add(new Parameter.Builder("CoolantTemp", "2101", new int[] {13}, "°С", "°С").multiplier(-0.525).offset(118.125).build());
        parameters.add(new Parameter.Builder("RPM", "2101", new int[] {20}, "rpm", "rpm").multiplier(32.0).build());
        parameters.add(new Parameter.Builder("Speed", "2101", new int[] {37}, "kph", "kph").build());
        parameters.add(new Parameter.Builder("InjectorPulseWidth", "2101", new int[] {21}, "ms", "ms").multiplier(1.02396).build());
        parameters.add(new Parameter.Builder("JRPMSxx", "2101", new int[] {28}, "rpm", "rpm").multiplier(16.0).build());
        parameters.add(new Parameter.Builder("Oxygen_b1s1", "2101", new int[] {29}, "В", "V").multiplier(0.0195).build());
        parameters.add(new Parameter.Builder("ThtrottlePosition", "2101", new int[] {38}, "%", "%").multiplier(0.392156).build());
        parameters.add(new Parameter.Builder("TimingAdvance", "2101", new int[] {15}, "degree", "degree").multiplier(0.37255).build());
        parameters.add(new Parameter.Builder("1#KnockControlIgnitionRetard", "2101", new int[] {15}, "°", "°").multiplier(0.37255).build());
        parameters.add(new Parameter.Builder("2#KnockControlIgnitionRetard", "2101", new int[] {16}, "°", "°").multiplier(0.37255).build());
        parameters.add(new Parameter.Builder("3#KnockControlIgnitionRetard", "2101", new int[] {17}, "°", "°").multiplier(0.37255).build());
        parameters.add(new Parameter.Builder("4#KnockControlIgnitionRetard", "2101", new int[] {18}, "°", "°").multiplier(0.37255).build());
        parameters.add(new Parameter.Builder("FSMxx", "2101", new int[] {30}, "шаг", "step").build());
        parameters.add(new Parameter.Builder("FuelSystemStatus_Trim", "2101", new int[] {35}, "бит", "bit").build());
        opel_VB_X18XE.super.setParameters(parameters);
    }
}
