package tirex.ezway.libs.ecu;

import tirex.ezway.libs.Parameter;

import java.util.ArrayList;

public class toyotagt86 extends DefaultAuto {
    public toyotagt86() {
        ArrayList<Parameter> parameters = new ArrayList<Parameter>();
        parameters.add(new Parameter.Builder("EngineOilTemp", "2101", new int[] {31}, "°С", "°С").offset(-40.0).build());
        toyotagt86.super.setParameters(parameters);
    }
}
