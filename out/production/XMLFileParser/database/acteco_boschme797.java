package tirex.ezway.libs.ecu;

import tirex.ezway.libs.Parameter;

import java.util.ArrayList;

public class acteco_boschme797 extends DefaultAuto {
    public acteco_boschme797() {
        ArrayList<Parameter> parameters = new ArrayList<Parameter>();
        parameters.add(new Parameter.Builder("FuelSystemStatus_Trim", "2101", new int[] {24}, "бит", "bit").build());
        parameters.add(new Parameter.Builder("RPM", "2101", new int[] {34, 35}, "rpm", "rpm").multiplier(0.25).build());
        parameters.add(new Parameter.Builder("Speed", "2101", new int[] {38}, "kph", "kph").build());
        parameters.add(new Parameter.Builder("VehicleAccelerate", "2101", new int[] {39}, "m/ss", "m/ss").multiplier(0.22).build());
        parameters.add(new Parameter.Builder("CoolantTempVolt", "2101", new int[] {40}, "В", "V").multiplier(0.02).build());
        parameters.add(new Parameter.Builder("CoolantTemp", "2101", new int[] {41}, "°С", "°С").offset(-40.0).build());
        parameters.add(new Parameter.Builder("IntakeAirTemp", "2101", new int[] {43}, "°С", "°С").offset(-40.0).build());
        parameters.add(new Parameter.Builder("MAF", "2101", new int[] {48, 49}, "kgperhour", "kgperhour").multiplier(0.1).build());
        parameters.add(new Parameter.Builder("InjectorPulseWidth", "2101", new int[] {66}, "мс", "msec").multiplier(0.1).build());
        parameters.add(new Parameter.Builder("TimingAdvance", "2101", new int[] {68}, "°", "°").multiplier(0.5).offset(-64.0).build());
        parameters.add(new Parameter.Builder("STFT1", "2101", new int[] {81}, "%", "%").multiplier(0.78125).offset(-100.0).build());
        parameters.add(new Parameter.Builder("LTFT1", "2101", new int[] {87}, "%", "%").multiplier(0.78125).offset(-100.0).build());
        parameters.add(new Parameter.Builder("Oxygen_b1s1", "2101", new int[] {83}, "В", "V").multiplier(0.02).build());
        parameters.add(new Parameter.Builder("Oxygen_b1s2", "2101", new int[] {84}, "В", "V").multiplier(0.02).build());
        parameters.add(new Parameter.Builder("ThrottlePosition", "2101", new int[] {57}, "%", "%").multiplier(0.390625).build());
        parameters.add(new Parameter.Builder("IntakeAirTemp2", "2101", new int[] {92}, "°С", "°С").multiplier(0.75).offset(-48.0).build());
        parameters.add(new Parameter.Builder("IntakeManifoldPressure", "2101", new int[] {93}, "кПа", "kPa").multiplier(0.5715).offset(24.0).build());
        acteco_boschme797.super.setParameters(parameters);
    }
}
