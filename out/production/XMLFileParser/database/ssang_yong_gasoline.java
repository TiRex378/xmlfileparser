package tirex.ezway.libs.ecu;

import tirex.ezway.libs.Parameter;

import java.util.ArrayList;

public class ssang_yong_gasoline extends DefaultAuto {
    public ssang_yong_gasoline() {
        ArrayList<Parameter> parameters = new ArrayList<Parameter>();
        parameters.add(new Parameter.Builder("CoolantTemp", "221101", new int[] {0}, "°С", "°С").offset(-60.0).build());
        parameters.add(new Parameter.Builder("IntakeAirTemp", "221103", new int[] {0}, "°С", "°С").offset(-60.0).build());
        parameters.add(new Parameter.Builder("RPM", "221104", new int[] {0, 1}, "rpm", "rpm").wordLE(true).build());
        parameters.add(new Parameter.Builder("RegRPM", "221105", new int[] {0, 1}, "rpm", "rpm").wordLE(true).build());
        parameters.add(new Parameter.Builder("EngineLoad", "221106", new int[] {0}, "%", "%").multiplier(0.5).build());
        parameters.add(new Parameter.Builder("MAF", "221107", new int[] {0, 1}, "kgperhour", "kgperhour").multiplier(0.1).wordLE(true).build());
        parameters.add(new Parameter.Builder("TP_Sens", "22110A", new int[] {0}, "%", "%").multiplier(0.390625).build());
        parameters.add(new Parameter.Builder("TimingAdvance", "22110B", new int[] {0, 1}, "degree", "degree").multiplier(0.1).wordLE(true).build());
        parameters.add(new Parameter.Builder("EngineTorque", "22110C", new int[] {0}, "Nm", "Nm").multiplier(3.0).build());
        parameters.add(new Parameter.Builder("InjectorPulseWidth", "22110D", new int[] {0, 1}, "ms", "ms").multiplier(0.008984375).wordLE(true).build());
        parameters.add(new Parameter.Builder("ControlModuleVoltage", "221110", new int[] {0}, "В", "V").multiplier(0.078125).build());
        parameters.add(new Parameter.Builder("Speed", "221113", new int[] {0}, "kph", "kph").multiplier(1.2).build());
        parameters.add(new Parameter.Builder("RearSpeed", "221114", new int[] {0, 1}, "kph", "kph").multiplier(0.015625).wordLE(true).build());
        parameters.add(new Parameter.Builder("Pedal1", "221115", new int[] {0, 1}, "В", "V").multiplier(0.0046875).wordLE(true).build());
        parameters.add(new Parameter.Builder("Pedal2", "221116", new int[] {0, 1}, "В", "V").multiplier(0.0046875).wordLE(true).build());
        parameters.add(new Parameter.Builder("ThrottlePosition", "221117", new int[] {0, 1}, "В", "V").multiplier(0.0046875).wordLE(true).build());
        parameters.add(new Parameter.Builder("ThrottlePosition2", "221118", new int[] {0, 1}, "В", "V").multiplier(0.0046875).wordLE(true).build());
        parameters.add(new Parameter.Builder("FuelInt 1", "22111B", new int[] {1}, "-", "-").multiplier(0.0078125).build());
        parameters.add(new Parameter.Builder("Oxygen_b1s1", "22111F", new int[] {0, 1}, "В", "V").multiplier(0.278).wordLE(true).build());
        parameters.add(new Parameter.Builder("IdlSwtch", "221147", new int[] {2}, "Выкл/Вкл", "Off/On").build());
        parameters.add(new Parameter.Builder("FullLoad", "221147", new int[] {2}, "Выкл/Вкл", "Off/On").build());
        parameters.add(new Parameter.Builder("ShftGear", "221147", new int[] {2}, "Выкл/Вкл", "Off/On").build());
        parameters.add(new Parameter.Builder("ACCntrl", "221147", new int[] {2}, "Выкл/Вкл", "Off/On").build());
        parameters.add(new Parameter.Builder("CltuchSwth", "221147", new int[] {2}, "Выкл/Вкл", "Off/On").build());
        parameters.add(new Parameter.Builder("CamActtr", "221148", new int[] {2}, "Выкл/Вкл", "Off/On").build());
        parameters.add(new Parameter.Builder("KnockCntl", "none", new int[] {2}, "Выкл/Вкл", "Off/On").build());
        parameters.add(new Parameter.Builder("ProtMiss", "221148", new int[] {2}, "Выкл/Вкл", "Off/On").build());
        parameters.add(new Parameter.Builder("PrgCntlVlv", "none", new int[] {2}, "Выкл/Вкл", "Off/On").build());
        parameters.add(new Parameter.Builder("LmbdFctn", "221148", new int[] {2}, "Выкл/Вкл", "Off/On").build());
        parameters.add(new Parameter.Builder("CatHeatng", "221149", new int[] {2}, "Выкл/Вкл", "Off/On").build());
        parameters.add(new Parameter.Builder("OvrnFlCut", "221149", new int[] {2}, "Выкл/Вкл", "Off/On").build());
        parameters.add(new Parameter.Builder("FulFlCut", "221149", new int[] {2}, "Выкл/Вкл", "Off/On").build());
        parameters.add(new Parameter.Builder("BrakeSW", "221149", new int[] {2}, "Выкл/Вкл", "Off/On").build());
        parameters.add(new Parameter.Builder("IdlRpmPN", "221150", new int[] {0}, "rpm", "rpm").offset(-128.0).build());
        parameters.add(new Parameter.Builder("IdlRpmD/R", "221151", new int[] {0}, "rpm", "rpm").offset(-128.0).build());
        ssang_yong_gasoline.super.setParameters(parameters);
    }
}
