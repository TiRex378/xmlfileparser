package tirex.ezway.libs.ecu;

import tirex.ezway.libs.Parameter;

import java.util.ArrayList;

public class opel_Z16XE extends DefaultAuto {
    public opel_Z16XE() {
        ArrayList<Parameter> parameters = new ArrayList<Parameter>();
        parameters.add(new Parameter.Builder("ControlModuleVoltage", "2101", new int[] {33}, "В", "V").multiplier(0.1).build());
        parameters.add(new Parameter.Builder("RPM", "2101", new int[] {34, 35}, "rpm", "rpm").multiplier(0.25).build());
        parameters.add(new Parameter.Builder("JRPMSxx", "2101", new int[] {37}, "rpm", "rpm").multiplier(12.5).build());
        parameters.add(new Parameter.Builder("Speed", "2101", new int[] {38}, "kph", "kph").build());
        parameters.add(new Parameter.Builder("CoolantTempVoltage", "2101", new int[] {40}, "В", "V").multiplier(0.01953125).build());
        parameters.add(new Parameter.Builder("CoolantTemp", "2101", new int[] {41}, "°С", "°С").offset(-40.0).build());
        parameters.add(new Parameter.Builder("IntakeAirTempVoltage", "2101", new int[] {42}, "В", "V").multiplier(0.01953125).build());
        parameters.add(new Parameter.Builder("IntakeAirTemp", "2101", new int[] {43}, "°С", "°С").offset(-40.0).build());
        parameters.add(new Parameter.Builder("MAPvolt", "2101", new int[] {48}, "В", "V").multiplier(0.01953125).build());
        parameters.add(new Parameter.Builder("IntakeManifoldPressure", "2101", new int[] {49}, "кПа", "kPa").multiplier(0.004078).build());
        parameters.add(new Parameter.Builder("PedalPositionVolt1", "2101", new int[] {50}, "В", "V").multiplier(0.01953125).build());
        parameters.add(new Parameter.Builder("PedalPositionVolt2", "2101", new int[] {51}, "В", "V").multiplier(0.01953125).build());
        parameters.add(new Parameter.Builder("PedalPosition", "2101", new int[] {53}, "%", "%").multiplier(0.390625).build());
        parameters.add(new Parameter.Builder("ThrottlePositionVoltage", "2101", new int[] {54}, "В", "V").multiplier(0.01953125).build());
        parameters.add(new Parameter.Builder("ThrottlePositionVoltage2", "2101", new int[] {55}, "В", "V").multiplier(0.01953125).build());
        parameters.add(new Parameter.Builder("ThrottlePosition", "2101", new int[] {57}, "%", "%").multiplier(0.390625).build());
        parameters.add(new Parameter.Builder("EGRPositionVolt", "2101", new int[] {60}, "В", "V").multiplier(0.01953125).build());
        parameters.add(new Parameter.Builder("EGRPosition", "2101", new int[] {61}, "%", "%").multiplier(0.390625).build());
        parameters.add(new Parameter.Builder("FuelTankVentilation", "2101", new int[] {62}, "%", "%").multiplier(0.390625).build());
        parameters.add(new Parameter.Builder("ECUFuelLevelVolt", "2101", new int[] {63}, "В", "V").multiplier(0.0585).build());
        parameters.add(new Parameter.Builder("ECUFuelLevel", "2101", new int[] {64}, "литр", "liter").multiplier(0.390625).build());
        parameters.add(new Parameter.Builder("IgnitionDwellAngle", "2101", new int[] {66}, "ms", "ms").multiplier(0.1).build());
        parameters.add(new Parameter.Builder("InjectorPulseWidth", "2101", new int[] {67}, "ms", "ms").multiplier(0.1).build());
        parameters.add(new Parameter.Builder("TimingAdvance", "2101", new int[] {68}, "degree", "degree").multiplier(0.5).offset(-64.0).build());
        parameters.add(new Parameter.Builder("DetUOZ_1", "2101", new int[] {78}, "degree", "degree").build());
        parameters.add(new Parameter.Builder("DetUOZ_2", "2101", new int[] {79}, "degree", "degree").build());
        parameters.add(new Parameter.Builder("DetUOZ_3", "2101", new int[] {80}, "degree", "degree").build());
        parameters.add(new Parameter.Builder("DetUOZ_4", "2101", new int[] {81}, "degree", "degree").build());
        parameters.add(new Parameter.Builder("STFT1", "2101", new int[] {84}, "%", "%").multiplier(0.78125).offset(-100.0).build());
        parameters.add(new Parameter.Builder("Oxygen_b1s1", "2101", new int[] {86}, "В", "V").multiplier(0.00434).build());
        parameters.add(new Parameter.Builder("Oxygen_b1s2", "2101", new int[] {87}, "В", "V").multiplier(0.00434).build());
        parameters.add(new Parameter.Builder("LTFT1", "2101", new int[] {93}, "%", "%").multiplier(0.78125).offset(-100.0).build());
        opel_Z16XE.super.setParameters(parameters);
    }
}
