package tirex.ezway.libs.ecu;

import tirex.ezway.libs.Parameter;

import java.util.ArrayList;

public class priusMHW11 extends DefaultAuto {
    public priusMHW11() {
        ArrayList<Parameter> parameters = new ArrayList<Parameter>();
        parameters.add(new Parameter.Builder("CoolantTemp", "0105", new int[] {0}, "°С", "°С").offset(-40.0).build());
        parameters.add(new Parameter.Builder("RPM", "010C", new int[] {0, 1}, "rpm", "rpm").multiplier(0.25).build());
        parameters.add(new Parameter.Builder("Speed", "010D", new int[] {0}, "kph", "kph").build());
        parameters.add(new Parameter.Builder("IntakeAirTemp", "010F", new int[] {0}, "°С", "°С").offset(-40.0).build());
        parameters.add(new Parameter.Builder("DistanceMIL", "0121", new int[] {0, 1}, "км", "km").build());
        parameters.add(new Parameter.Builder("MG2_RPM", "01C2", new int[] {2, 3}, "rpm", "rpm").build());
        parameters.add(new Parameter.Builder("MG2_Torque", "01C2", new int[] {4, 5}, "Nm", "Nm").multiplier(0.25).build());
        parameters.add(new Parameter.Builder("MG1_RPM", "01C4", new int[] {2, 3}, "rpm", "rpm").build());
        parameters.add(new Parameter.Builder("MG1_Torque", "01C4", new int[] {4, 5}, "Nm", "Nm").multiplier(0.25).build());
        parameters.add(new Parameter.Builder("RequestPower", "01C5", new int[] {0, 1}, "W", "W").build());
        parameters.add(new Parameter.Builder("TargetRevol", "01C6", new int[] {0, 1}, "rpm", "rpm").build());
        parameters.add(new Parameter.Builder("EngineSpeedSim", "01C7", new int[] {0, 1}, "rpm", "rpm").build());
        parameters.add(new Parameter.Builder("ControlTorque", "01C8", new int[] {0, 1}, "Nm", "Nm").multiplier(4.0).build());
        parameters.add(new Parameter.Builder("SoC", "01C9", new int[] {0}, "%", "%").multiplier(0.392).build());
        parameters.add(new Parameter.Builder("WOUTControlPower", "01CA", new int[] {2}, "W", "W").multiplier(160.0).build());
        parameters.add(new Parameter.Builder("WINControlPower", "01CA", new int[] {3}, "W", "W").multiplier(160.0).build());
        parameters.add(new Parameter.Builder("DchrgReqSoC", "01CB", new int[] {0, 1}, "W", "W").multiplier(160.0).build());
        parameters.add(new Parameter.Builder("MG1_InverterTemperature", "01CD", new int[] {2}, "°С", "°С").offset(-50.0).build());
        parameters.add(new Parameter.Builder("MG2_InverterTemperature", "01CD", new int[] {3}, "°С", "°С").offset(-50.0).build());
        parameters.add(new Parameter.Builder("MG1_Temperature", "01CE", new int[] {2}, "°С", "°С").offset(-50.0).build());
        parameters.add(new Parameter.Builder("MG2_Temperature", "01CE", new int[] {3}, "°С", "°С").offset(-50.0).build());
        parameters.add(new Parameter.Builder("MotorCurrentV", "01CF", new int[] {2, 3}, "A", "A").multiplier(1.466).build());
        parameters.add(new Parameter.Builder("MotorCurrentW", "01CF", new int[] {4, 5}, "A", "A").multiplier(1.466).build());
        parameters.add(new Parameter.Builder("GeneratorCurrentV", "01D0", new int[] {2, 3}, "A", "A").multiplier(0.488).build());
        parameters.add(new Parameter.Builder("GeneratorCurrentW", "01D0", new int[] {4, 5}, "A", "A").multiplier(0.488).build());
        parameters.add(new Parameter.Builder("HV_BatteryVoltage", "01D1", new int[] {0}, "В", "V").multiplier(2.0).build());
        parameters.add(new Parameter.Builder("HV_BatteryCurrent", "01D2", new int[] {0, 1}, "A", "A").multiplier(2.0).build());
        parameters.add(new Parameter.Builder("ShiftSensor2", "01D3", new int[] {0, 1}, "В", "V").multiplier(0.0196).build());
        parameters.add(new Parameter.Builder("AccelerationSensorMain", "01D5", new int[] {0}, "В", "V").multiplier(0.01953125).build());
        parameters.add(new Parameter.Builder("AccelerationSensorSub", "01D5", new int[] {1}, "В", "V").multiplier(0.01953125).build());
        parameters.add(new Parameter.Builder("BatteryTempMax", "01D6", new int[] {0}, "°С", "°С").build());
        parameters.add(new Parameter.Builder("BatteryTempMin", "01D6", new int[] {1}, "°С", "°С").build());
        parameters.add(new Parameter.Builder("VhclSpdSimKPH", "01D7", new int[] {0}, "kph", "kph").multiplier(2.0).build());
        parameters.add(new Parameter.Builder("AuxBattVolt", "01D8", new int[] {0}, "В", "V").multiplier(0.078125).build());
        priusMHW11.super.setParameters(parameters);
    }
}
