package tirex.ezway.libs.ecu;

import tirex.ezway.libs.Parameter;

import java.util.ArrayList;

public class vaz_boschmp7 extends DefaultAuto {
    public vaz_boschmp7() {
        ArrayList<Parameter> parameters = new ArrayList<Parameter>();
        parameters.add(new Parameter.Builder("MAF_Voltage", "2101", new int[] {9}, "В", "V").multiplier(0.019535).build());
        parameters.add(new Parameter.Builder("TL", "2101", new int[] {10}, "ms", "ms").multiplier(0.05).build());
        parameters.add(new Parameter.Builder("ControlModuleVoltage", "2101", new int[] {11}, "В", "V").multiplier(0.0942).build());
        parameters.add(new Parameter.Builder("CoolantTemp", "2101", new int[] {12}, "°С", "°С").multiplier(0.75).offset(-48.0).build());
        parameters.add(new Parameter.Builder("TimingAdvance", "2101", new int[] {13}, "degree", "degree").multiplier(-0.75).offset(108.0).build());
        parameters.add(new Parameter.Builder("DetUOZ_1", "2101", new int[] {14}, "°", "°").multiplier(0.75).build());
        parameters.add(new Parameter.Builder("ThrottlePosition", "2101", new int[] {15}, "%", "%").multiplier(0.390625).build());
        parameters.add(new Parameter.Builder("Speed", "2101", new int[] {16}, "kph", "kph").build());
        parameters.add(new Parameter.Builder("RPM", "2101", new int[] {17}, "rpm", "rpm").multiplier(40.0).build());
        parameters.add(new Parameter.Builder("InjectorPulseWidth", "2101", new int[] {18, 19}, "ms", "ms").multiplier(0.024).build());
        parameters.add(new Parameter.Builder("FSMxx", "2101", new int[] {20}, "шаг", "step").build());
        parameters.add(new Parameter.Builder("RPM_10", "2101", new int[] {21}, "rpm", "rpm").multiplier(10.0).build());
        parameters.add(new Parameter.Builder("JRPMSxx", "2101", new int[] {24}, "rpm", "rpm").multiplier(10.0).build());
        parameters.add(new Parameter.Builder("MAF_XX", "2101", new int[] {25}, "kgperhour", "kgperhour").multiplier(0.25).build());
        parameters.add(new Parameter.Builder("MAF", "2101", new int[] {26}, "kgperhour", "kgperhour").multiplier(0.25).build());
        parameters.add(new Parameter.Builder("Oxygen_b1s1", "2101", new int[] {27}, "В", "V").multiplier(0.0044).build());
        parameters.add(new Parameter.Builder("TRA", "2101", new int[] {29}, "ms", "ms").build());
        parameters.add(new Parameter.Builder("Adsorber", "2101", new int[] {31}, "%", "%").multiplier(0.390625).build());
        parameters.add(new Parameter.Builder("Oxygen_b1s2", "2101", new int[] {35}, "В", "V").multiplier(0.0044).build());
        vaz_boschmp7.super.setParameters(parameters);
    }
}
