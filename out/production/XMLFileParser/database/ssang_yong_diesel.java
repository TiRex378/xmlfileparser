package tirex.ezway.libs.ecu;

import tirex.ezway.libs.Parameter;

import java.util.ArrayList;

public class ssang_yong_diesel extends DefaultAuto {
    public ssang_yong_diesel() {
        ArrayList<Parameter> parameters = new ArrayList<Parameter>();
        parameters.add(new Parameter.Builder("CoolantTemp", "21B1", new int[] {10}, "°С", "°С").offset(-40.0).build());
        parameters.add(new Parameter.Builder("ThrottlePosition", "2101", new int[] {12}, "%", "%").build());
        parameters.add(new Parameter.Builder("RPM", "2101", new int[] {13}, "rpm", "rpm").multiplier(40.0).build());
        parameters.add(new Parameter.Builder("TimingAdvance", "2101", new int[] {18}, "degree", "degree").multiplier(0.5).build());
        parameters.add(new Parameter.Builder("Speed", "2101", new int[] {22}, "kph", "kph").build());
        parameters.add(new Parameter.Builder("ControlModuleVoltage", "2101", new int[] {20}, "В", "V").multiplier(0.05).offset(5.2).build());
        parameters.add(new Parameter.Builder("InjectorPulseWidth", "2101", new int[] {24, 25}, "ms", "ms").multiplier(0.008).wordLE(true).build());
        parameters.add(new Parameter.Builder("MAF", "2101", new int[] {26, 27}, "grpersec", "grpersec").multiplier(0.0277777).wordLE(true).build());
        parameters.add(new Parameter.Builder("LitersPerHour", "2101", new int[] {30, 31}, "lphour", "lphour").multiplier(0.02).wordLE(true).build());
        parameters.add(new Parameter.Builder("FuelEconomy", "2101", new int[] {32, 33}, "lph", "lph").multiplier(0.0078125).wordLE(true).build());
        parameters.add(new Parameter.Builder("FuelSystemStatus_Trim", "2101", new int[] {4}, "бит", "bit").build());
        parameters.add(new Parameter.Builder("DetonationStatus", "2101", new int[] {5}, "бит", "bit").build());
        ssang_yong_diesel.super.setParameters(parameters);
    }
}
