package tirex.ezway.libs.ecu;

import tirex.ezway.libs.Parameter;

import java.util.ArrayList;

public class mazda extends DefaultAuto {
    public mazda() {
        ArrayList<Parameter> parameters = new ArrayList<Parameter>();
        parameters.add(new Parameter.Builder("TFT2", "2217B3", new int[] {1}, "fahrenheit", "fahrenheit").multiplier(1.68).offset(-57.0).build());
        parameters.add(new Parameter.Builder("WT1", "22C902", new int[] {3}, "fahrenheit", "fahrenheit").multiplier(1.8).offset(-58.0).build());
        parameters.add(new Parameter.Builder("WT2", "22C902", new int[] {4}, "fahrenheit", "fahrenheit").multiplier(1.8).offset(-58.0).build());
        parameters.add(new Parameter.Builder("WT3", "22C902", new int[] {5}, "fahrenheit", "fahrenheit").multiplier(1.8).offset(-58.0).build());
        parameters.add(new Parameter.Builder("WT4", "22C902", new int[] {6}, "fahrenheit", "fahrenheit").multiplier(1.8).offset(-58.0).build());
        parameters.add(new Parameter.Builder("MeasuredPassengerWeight", "22596A", new int[] {1, 2}, "кг", "kg").multiplier(0.0039).build());
        parameters.add(new Parameter.Builder("ActualValveTiming", "2216CD", new int[] {1, 2}, "degrees", "degrees").build());
        parameters.add(new Parameter.Builder("ActualValveTimingCS", "2216CD", new int[] {1, 2}, "degrees", "degrees").multiplier(0.0625).build());
        parameters.add(new Parameter.Builder("FuelInjectorTime", "221410", new int[] {1, 2}, "ms", "ms").multiplier(0.008).build());
        parameters.add(new Parameter.Builder("EVAP_System", "22002E", new int[] {1}, "%", "%").multiplier(0.392157).build());
        parameters.add(new Parameter.Builder("EGRValvePosition", "22002C", new int[] {1}, "%", "%").multiplier(0.392157).build());
        parameters.add(new Parameter.Builder("IntakeManifoldVacuum", "010b", new int[] {0}, "кПа", "kPa").build());
        mazda.super.setParameters(parameters);
    }
}
