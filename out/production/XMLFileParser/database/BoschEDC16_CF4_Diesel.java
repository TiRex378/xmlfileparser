package tirex.ezway.libs.ecu;

import tirex.ezway.libs.Parameter;

import java.util.ArrayList;

public class BoschEDC16_CF4_Diesel extends DefaultAuto {
    public BoschEDC16_CF4_Diesel() {
        ArrayList<Parameter> parameters = new ArrayList<Parameter>();
        parameters.add(new Parameter.Builder("RPM", "2130", new int[] {0, 1}, "rpm", "rpm").multiplier(0.25).build());
        parameters.add(new Parameter.Builder("Speed", "2131", new int[] {0, 1}, "kph", "kph").multiplier(0.0078125).build());
        parameters.add(new Parameter.Builder("PedalPosition", "213E", new int[] {0, 1}, "%", "%").multiplier(0.0015259).build());
        parameters.add(new Parameter.Builder("LitersPerHour", "2142", new int[] {0, 1}, "lphour", "lphour").multiplier(4.0E-4).build());
        parameters.add(new Parameter.Builder("ECUFuelLevel", "2143", new int[] {0, 1}, "%", "%").multiplier(0.0015259).build());
        parameters.add(new Parameter.Builder("CoolantTemp", "2150", new int[] {0, 1}, "°С", "°С").multiplier(0.02).offset(-40.0).build());
        parameters.add(new Parameter.Builder("IntakeAirTempTurbo", "2152", new int[] {0, 1}, "°С", "°С").multiplier(0.02).offset(-40.0).build());
        parameters.add(new Parameter.Builder("IntakeAirTemp", "215e", new int[] {0, 1}, "°С", "°С").multiplier(0.02).offset(-40.0).build());
        parameters.add(new Parameter.Builder("CycleConsumption", "2167", new int[] {0, 1}, "mmpc", "mmpc").multiplier(0.003).build());
        parameters.add(new Parameter.Builder("FuelTemp", "2175", new int[] {0, 1}, "°С", "°С").multiplier(0.02).offset(-40.0).build());
        parameters.add(new Parameter.Builder("FuelPressure", "21A0", new int[] {0, 1}, "кПа", "kPa").multiplier(5.0).build());
        parameters.add(new Parameter.Builder("OilLevel", "21AB", new int[] {0, 1}, "%", "%").multiplier(0.0015259).build());
        parameters.add(new Parameter.Builder("ThrottlePosition", "21AD", new int[] {0, 1}, "%", "%").multiplier(0.0015259).build());
        parameters.add(new Parameter.Builder("BaroPressure", "21B1", new int[] {0, 1}, "кПа", "kPa").multiplier(0.1).build());
        parameters.add(new Parameter.Builder("Oxygen_b1s1", "21E3", new int[] {0, 1}, "mВ", "mV").build());
        parameters.add(new Parameter.Builder("LambdaO2", "21E4", new int[] {0, 1}, "%", "%").multiplier(0.0015259).build());
        BoschEDC16_CF4_Diesel.super.setParameters(parameters);
    }
}
