package tirex.ezway.libs.ecu;

import tirex.ezway.libs.Parameter;

import java.util.ArrayList;

public class vaz_yanvar51 extends DefaultAuto {
    public vaz_yanvar51() {
        ArrayList<Parameter> parameters = new ArrayList<Parameter>();
        parameters.add(new Parameter.Builder("CoolantTemp", "2101", new int[] {10}, "°С", "°С").offset(-40.0).build());
        parameters.add(new Parameter.Builder("ThrottlePosition", "2101", new int[] {12}, "%", "%").build());
        parameters.add(new Parameter.Builder("RPM", "2101", new int[] {13}, "rpm", "rpm").multiplier(40.0).build());
        parameters.add(new Parameter.Builder("RPMXX", "2101", new int[] {14}, "rpm", "rpm").multiplier(10.0).build());
        parameters.add(new Parameter.Builder("SSMxx", "2101", new int[] {15}, "шаг", "step").build());
        parameters.add(new Parameter.Builder("FSMxx", "2101", new int[] {16}, "шаг", "step").build());
        parameters.add(new Parameter.Builder("TimingAdvance", "2101", new int[] {18}, "degree", "degree").multiplier(0.5).build());
        parameters.add(new Parameter.Builder("Speed", "2101", new int[] {19}, "kph", "kph").build());
        parameters.add(new Parameter.Builder("ControlModuleVoltage", "2101", new int[] {20}, "В", "V").multiplier(0.05).offset(5.2).build());
        parameters.add(new Parameter.Builder("JRPMSxx", "2101", new int[] {21}, "rpm", "rpm").multiplier(10.0).build());
        parameters.add(new Parameter.Builder("InjectorPulseWidth", "2101", new int[] {22, 23}, "ms", "ms").multiplier(0.008).wordLE(true).build());
        parameters.add(new Parameter.Builder("MAF", "2101", new int[] {24, 25}, "kgperhour", "kgperhour").multiplier(0.1).wordLE(true).build());
        parameters.add(new Parameter.Builder("CyclicAirflow", "2101", new int[] {26, 27}, "mgpc", "mgpc").multiplier(0.16666667).wordLE(true).build());
        parameters.add(new Parameter.Builder("LitersPerHour", "2101", new int[] {28, 29}, "lphour", "lphour").multiplier(0.02).wordLE(true).build());
        parameters.add(new Parameter.Builder("FuelEconomy", "2101", new int[] {30, 31}, "lph", "lph").multiplier(0.0078125).wordLE(true).build());
        parameters.add(new Parameter.Builder("FuelSystemStatus_Trim", "2101", new int[] {4}, "бит", "bit").build());
        parameters.add(new Parameter.Builder("ADC_Detonation", "2103", new int[] {2}, "В", "V").multiplier(0.01953125).build());
        parameters.add(new Parameter.Builder("ADC_CoolantTemp", "2103", new int[] {3}, "В", "V").multiplier(0.01953125).build());
        parameters.add(new Parameter.Builder("ADC_MAF", "2103", new int[] {4}, "В", "V").multiplier(0.01953125).build());
        parameters.add(new Parameter.Builder("ADC_SystemVoltage", "2103", new int[] {5}, "В", "V").multiplier(0.090907).build());
        parameters.add(new Parameter.Builder("ADC_RCO", "2103", new int[] {6}, "В", "V").multiplier(0.0195312).build());
        parameters.add(new Parameter.Builder("ADC_ThrottlePosition", "2103", new int[] {7}, "В", "V").multiplier(0.0195312).build());
        vaz_yanvar51.super.setParameters(parameters);
    }
}
