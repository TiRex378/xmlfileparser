package tirex.ezway.libs.ecu;

import tirex.ezway.libs.Parameter;

import java.util.ArrayList;

public class vaz_M74Can extends DefaultAuto {
    public vaz_M74Can() {
        ArrayList<Parameter> parameters = new ArrayList<Parameter>();
        parameters.add(new Parameter.Builder("ControlModuleVoltage", "220001", new int[] {4}, "В", "V").multiplier(0.1).build());
        parameters.add(new Parameter.Builder("CoolantTemp", "220001", new int[] {5, 6}, "°С", "°С").multiplier(0.75).offset(-48.0).build());
        parameters.add(new Parameter.Builder("RPM", "220001", new int[] {7, 8}, "rpm", "rpm").multiplier(0.25).build());
        parameters.add(new Parameter.Builder("Speed", "220001", new int[] {9}, "kph", "kph").multiplier(1.25).build());
        parameters.add(new Parameter.Builder("TimingAdvance", "220001", new int[] {10}, "degree", "degree").multiplier(0.75).build());
        parameters.add(new Parameter.Builder("InjectorPulseWidth", "220001", new int[] {11, 12}, "ms", "ms").multiplier(0.00634).build());
        parameters.add(new Parameter.Builder("ThrottlePosition", "220001", new int[] {13, 14}, "%", "%").multiplier(0.02441406).build());
        parameters.add(new Parameter.Builder("PedalPosition", "220001", new int[] {15, 16}, "%", "%").multiplier(0.0015259).build());
        parameters.add(new Parameter.Builder("EngineLoad", "220001", new int[] {17, 18}, "%", "%").multiplier(0.0234375).build());
        parameters.add(new Parameter.Builder("MAF", "220001", new int[] {19, 20}, "kgperhour", "kgperhour").multiplier(0.1).build());
        parameters.add(new Parameter.Builder("Oxygen_b1s1", "220001", new int[] {22}, "В", "V").multiplier(0.00502).build());
        parameters.add(new Parameter.Builder("Oxygen_b1s1_period", "220001", new int[] {23}, "ms", "ms").multiplier(0.00502).build());
        parameters.add(new Parameter.Builder("Oxygen_b1s2", "220001", new int[] {24}, "В", "V").multiplier(0.00502).build());
        parameters.add(new Parameter.Builder("AltitudeCorrection", "220001", new int[] {28, 29}, "кг", "kg").multiplier(4.0E-4).build());
        parameters.add(new Parameter.Builder("LTFT1", "220001", new int[] {30, 31}, "%", "%").multiplier(3.05176E-5).build());
        parameters.add(new Parameter.Builder("LitersPerHour", "220001", new int[] {34, 35}, "lphour", "lphour").multiplier(0.00215).build());
        parameters.add(new Parameter.Builder("IntakeAirTemp", "220001", new int[] {36}, "°С", "°С").multiplier(0.75).offset(-48.0).build());
        parameters.add(new Parameter.Builder("CoefficientCanisterBleed", "220001", new int[] {37, 38}, "%", "%").multiplier(0.001526).build());
        parameters.add(new Parameter.Builder("DetUOZ", "220001", new int[] {41}, "degree", "degree").multiplier(0.75).build());
        parameters.add(new Parameter.Builder("JRPMSxx", "220001", new int[] {42}, "rpm", "rpm").multiplier(10.0).build());
        parameters.add(new Parameter.Builder("DifTorqueAdapted", "220001", new int[] {43, 44}, "%", "%").multiplier(0.0030519).build());
        parameters.add(new Parameter.Builder("AirLeakageThrottle", "220001", new int[] {45, 46}, "kgperhour", "kgperhour").multiplier(0.1).build());
        parameters.add(new Parameter.Builder("RegulationXXI", "220001", new int[] {47, 48}, "%", "%").multiplier(0.0030519).build());
        parameters.add(new Parameter.Builder("RegulationXXPD", "220001", new int[] {49, 50}, "%", "%").multiplier(0.0030519).build());
        parameters.add(new Parameter.Builder("IntakeCoolantTemp", "220001", new int[] {51}, "°С", "°С").multiplier(0.75).offset(-48.0).build());
        parameters.add(new Parameter.Builder("ECUtime", "Core", new int[] {57, 58}, "минута", "minute").multiplier(0.166667).build());
        parameters.add(new Parameter.Builder("FuelSystemStatus_Trim", "220001", new int[] {56}, "бит", "bit").build());
        parameters.add(new Parameter.Builder("DetonationStatus", "220001", new int[] {56}, "бит", "bit").build());
        parameters.add(new Parameter.Builder("ADC_SystemVoltage", "220003", new int[] {3}, "В", "V").multiplier(0.09421875).build());
        parameters.add(new Parameter.Builder("ADC_CoolantTemp", "220003", new int[] {4}, "В", "V").multiplier(0.01953125).build());
        parameters.add(new Parameter.Builder("ADC_IntakeAirTemp", "220003", new int[] {5}, "В", "V").multiplier(0.01953125).build());
        parameters.add(new Parameter.Builder("ADC_MAFPeriod", "220003", new int[] {6, 7}, "ms", "ms").multiplier(0.1).build());
        parameters.add(new Parameter.Builder("ADC_ThrottlePosition", "220003", new int[] {8, 9}, "В", "V").multiplier(0.0012207).build());
        parameters.add(new Parameter.Builder("ADC_ThrottlePosition2", "220003", new int[] {10, 11}, "В", "V").multiplier(0.0012207).build());
        parameters.add(new Parameter.Builder("ADC_AccelPosition", "220003", new int[] {12, 13}, "ms", "ms").multiplier(0.003224).build());
        parameters.add(new Parameter.Builder("ADC_AccelPosition2", "220003", new int[] {14, 15}, "ms", "ms").multiplier(0.003224).build());
        parameters.add(new Parameter.Builder("ADC_Oxygen_b1s1", "220003", new int[] {16}, "В", "V").multiplier(0.025).build());
        parameters.add(new Parameter.Builder("ADC_Oxygen_b1s1_OM", "220003", new int[] {17, 18}, "Om", "Om").multiplier(2.0).build());
        parameters.add(new Parameter.Builder("ADC_Oxygen_b1s2", "220003", new int[] {19}, "В", "V").multiplier(0.025).build());
        parameters.add(new Parameter.Builder("ADC_Oxygen_b1s2_OM", "220003", new int[] {20, 21}, "Om", "Om").multiplier(2.0).build());
        parameters.add(new Parameter.Builder("ADC_Detonation", "220003", new int[] {22}, "В", "V").multiplier(0.01953125).build());
        vaz_M74Can.super.setParameters(parameters);
    }
}
