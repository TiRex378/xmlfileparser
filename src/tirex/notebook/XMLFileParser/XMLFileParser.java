package tirex.notebook.XMLFileParser;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.swing.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class XMLFileParser {

    public static void main(String[] args) {
        final JFrame frame=new JFrame("XMLFileParser");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

//--------------------------------------------------------------------------------------------------------------------//
        //     За основу взята заготовка, которая была выполнена в качестве тестового задания - На экран выводится форма
        // с текстом и кнопкой посередине. Также доступно меню с кнопками "О разработчиках" и "Выйти".
        //     Данная программа при нажатии кнопки "Start to parsing" приводит в действие механизм парсинга файлов. При
        // этом в папку с текущим скомпилированным файлом (out/production/XMLFileParser) проекта должны быть помещены
        // файлы *.ecuxml, подготовленные нужным образом. В результате работы программы в текущей директории создаётся
        // папка database, которая содержит сформированные программой файлы *.java, а также файл log.txt, в который
        // записываются ошибки, найденные в ходе работы программы.
//--------------------------------------------------------------------------------------------------------------------//

        final JMenuBar menuBar = new JMenuBar();
        JMenu fileMenu = new JMenu("Меню");
        menuBar.add(fileMenu);
        JMenuItem menuItemInfo = new JMenuItem("О разработчиках");
        menuItemInfo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                final JFrame frameInfo = new JFrame("О разработчиках:");
                JPanel panelInfo = new JPanel();
                panelInfo.setLayout(new GridBagLayout());
                JPanel panelContent = new JPanel();
                panelContent.setLayout(new BoxLayout(panelContent,BoxLayout.PAGE_AXIS));
                panelContent.add(new JLabel("Разработчик программы:"));
                panelContent.add(new JLabel("Оболенсков А.Г."));
                panelContent.add(new JLabel("obolenskov@kipop.ru"));
                panelInfo.add(panelContent);
                frameInfo.add(panelInfo);
                frameInfo.setPreferredSize(new Dimension(200,100));
                frameInfo.pack();
                frameInfo.setLocationRelativeTo(null);
                frameInfo.setVisible(true);
            }
        });
        JMenuItem menuItemExit = new JMenuItem("Выйти");
        menuItemExit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.dispose();
            }
        });
        fileMenu.add(menuItemInfo);
        fileMenu.add(menuItemExit);
        frame.setJMenuBar(menuBar);

        JPanel panelInfo = new JPanel();
        panelInfo.setLayout(new GridBagLayout());
        JPanel panelContentInfo = new JPanel();
        panelContentInfo.setLayout(new BoxLayout(panelContentInfo,BoxLayout.PAGE_AXIS));
        final JLabel textInfo = new JLabel("Выберите действие:");
        panelContentInfo.add(textInfo);
        panelInfo.add(panelContentInfo);
        panelInfo.setPreferredSize(new Dimension(220,40));

        JPanel panelBody = new JPanel();
        panelBody.setLayout(new BoxLayout(panelBody,BoxLayout.PAGE_AXIS));
        JPanel panelButton = new JPanel();
        panelButton.setLayout(new GridBagLayout());
        JPanel panelContent = new JPanel();
        panelContent.setLayout(new BoxLayout(panelContent,BoxLayout.PAGE_AXIS));
        JButton buttonParseXMLFiles = new JButton("Start to parsing");
        buttonParseXMLFiles.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // 1) Получим путь к текущему файлу
                /*1*/
                filePath = getClass().getProtectionDomain().getCodeSource().getLocation().getPath();

                // 2) Получим текущую директорию
                int numberOfSeparators = 0;
                for (int i = 0; i < filePath.length(); i++)
                    if (filePath.substring(i, i + 1).equals("/")) numberOfSeparators++;
                folder = "";
                int targetSeparatorNumber = 0;
                for (int i = 0; i < filePath.length(); i++) {
                    String s = filePath.substring(i, i + 1);
                    if (s.equals("/"))
                        targetSeparatorNumber++;
                    if (targetSeparatorNumber == numberOfSeparators)
                        break;
                    else
                        folder += s;
                }
                /*2*/
                folder += "/";

                // 4) Получим список xml файлов в текущей директории
                files = new File(folder).listFiles(new FilenameFilter() {
                    @Override
                    public boolean accept(File dir, String name) {
                        return name.endsWith(".ecuxml");
                    }
                });
                messageListOfFiles = "";
                for (File file : files) messageListOfFiles += file.getName() + ", ";
                if (messageListOfFiles.equals(""))
                    textInfo.setText("Поместите исходные xml файлы в текущую папку");
                else {
                    // 3) Создадим в текущей директории каталог для выходных файлов *.java
                    newFolder = new File(folder, "database");
                    if (!newFolder.exists()) {
                        /*3*/
                        folderDatabaseWasCreated = "Folder \"database\" was created";
                        /*4*/
                        newFolder.mkdir();
                    }


                    /*6*/
                    numberOfXMLFiles = files.length;
                    String message = "";
                    textInfo.setText("Число распознанных файлов - " + numberOfXMLFiles);

                    globalValues = new ArrayList<String>();
                    fileNames = new ArrayList<String>();

                    for (File file : files) {
                        createNewJavaFile(file.getName());
                    }

                    try {
                        autoListWriter = new PrintWriter(newFolder + "/AutoList.java", "UTF-8");
                        autoListWriter.println("package tirex.ezway.libs.ecu;");
                        autoListWriter.println();
                        autoListWriter.println("public class AutoList {");
                        autoListWriter.println("    private static final String[] autoList = {");

                        String endOfLine = ",";
                        for (int i=0;i<fileNames.size();i++) {
                            if (i == fileNames.size()-1)
                                endOfLine = "};";
                            autoListWriter.println("        \"" + fileNames.get(i) + "\"" + endOfLine);
                        }

                        autoListWriter.println("    public static DefaultAuto getAutoByNumber(int autoNumber) throws IndexOutOfBoundsException{");
                        autoListWriter.println("        switch (autoNumber) {");

                        for (int i=0;i<fileNames.size();i++)
                            autoListWriter.println("        case " + i + ": return new " + fileNames.get(i) + "();");

                        autoListWriter.println("        }");
                        autoListWriter.println("        throw  new IndexOutOfBoundsException(\"There isn't auto with such number\");");
                        autoListWriter.println("    }");
                        autoListWriter.println("    public static String[] getAutoList() {");
                        autoListWriter.println("        return autoList;");
                        autoListWriter.println("    }");
                        autoListWriter.println("}");

                        autoListWriter.close();

                            } catch (FileNotFoundException e1) {
                        e1.printStackTrace();
                    } catch (UnsupportedEncodingException e1) {
                        e1.printStackTrace();
                    }


                    saveLogs();
                }
            }
        });
        panelContent.add(buttonParseXMLFiles);
        panelButton.add(panelContent);
        panelButton.setPreferredSize(new Dimension(350, 70));

        panelBody.add(panelInfo);
        panelBody.add(panelButton);

        frame.add(panelBody);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true) ;
    }

    private static byte value;
    private static String stringValue;
    private static HashMap<String, ArrayList<ParsingParameter>> includesHashMap;
    private static ArrayList<String> includes;
    private static ArrayList<String> init_string;
    private static ArrayList<String> init_stringValues;
    private static PrintWriter writer;
    private static PrintWriter autoListWriter;
    private static ArrayList<String> globalValues;
    private static ArrayList<String> fileNames; // Existed file names

    private static void printIncludeParameters() {
        if (includes.size() != 0) {
            String[] keys = new String[includesHashMap.size()];
            int keyNumber = 0;
            for (String key : includesHashMap.keySet()) {
                keys[keyNumber] = key;
                keyNumber++;
            }
            for (int i = 0; i < includesHashMap.size(); i++) {
                writer.println("include=" + keys[i]);
                for (int j = 0; j < includesHashMap.get(keys[i]).size(); j++) {
                    writer.println("    " + includesHashMap.get(keys[i]).get(j).toString());
                }
            }
            writer.println();
        }
    }

    private static void createNewJavaFile(String fileName) {
        String originalFileName = fileName;
        fileName = checkFileNameForWrongLetters(fileName);

        // Поскольку зарезервированные языком слова недопустимы в качестве имени класса, удостоверимся, что название
        // нашего файла не входит в этот список
        String fileTitle = fileName.substring(0,fileName.length()-7);
        if (!isJavaKeyword(fileTitle)) {

            try {
                // Check the include sector of the file and save all include parameters
                includes = parseDocumentAndSearchSectionByName(parseXmlFile(originalFileName), "include");
//                // Check the init-string sector of the file and save all init-string parameters
                init_string = new ArrayList<String>();
                init_stringValues = new ArrayList<String>();
                try {
                    init_string = parseDocumentAndSearchSectionByName(parseXmlFile(originalFileName), "init-string");
                    init_stringValues = getInit_stringValues(init_string.get(0));
                } catch (IndexOutOfBoundsException e) {
                }
                // Create new HashMap for all connected files
                includesHashMap = new HashMap<String, ArrayList<ParsingParameter>>();
                String includesToString = "";
                if (includes.size() != 0) {
                    for (int i=0;i<includes.size();i++) {
                        if (i!=0) includesToString += ", ";
                        includesToString += String.valueOf(includes.get(i));
                    }
                    // For each file from includes list
                    for (String include : includes)
                        includesHashMap.put(String.valueOf(include), parseDocumentAndSearchParameters(parseXmlFile(String.valueOf(include)),null));
                }

                // Get the parsingParametersArrayList for target file
                ArrayList<ParsingParameter> parsingParameters = parseDocumentAndSearchParameters(parseXmlFile(originalFileName), includesHashMap);

                boolean canCreateJavaFile;
                // Определим случаи, в которых файл нет смысла создавать
                // 1) Если это библиотека, в которой просто нет параметров
                canCreateJavaFile = (parsingParameters.size() != 0);
                // Остальные случаи можно будет добавить позже

                if (canCreateJavaFile) {
                    // Create the new class for the target auto
                    writer = new PrintWriter(newFolder + "/" + fileTitle + ".java", "UTF-8");

//-----------------------------------Print includes in the header of target file--------------------------------------//
//                    printIncludeParameters();
//--------------------------------------------------------------------------------------------------------------------//

                    writer.println("package tirex.ezway.libs.ecu;");
                    writer.println();
                    writer.println("import tirex.ezway.libs.Parameter;");
                    writer.println();
                    writer.println("import java.util.ArrayList;");
                    writer.println();
                    writer.println("public class " + fileTitle + " extends DefaultAuto {");
                    writer.println("    public " + fileTitle + "() {");
                    writer.println("        ArrayList<Parameter> parameters = new ArrayList<Parameter>();");

//---------------------------------Print to log file list of existed includes-----------------------------------------//
//                globalValues.add(String.valueOf("FileName="+fileTitle + " Number_of_Includes="+includes.size()+ " " + includesToString));
//--------------------------------------------------------------------------------------------------------------------//
                    for (int i = 0; i < parsingParameters.size(); i++) {
//----------------------------The output in the log the list of existing dimensions-----------------------------------//
//                    globalValues.add(String.valueOf(parsingParameters.get(i).getUnit()));
//--------------------------------------------------------------------------------------------------------------------//
//-------------------------------------Defining values remain unchanged-----------------------------------------------//
                        String targetBaseRaw = parsingParameters.get(i).getBase_raw();
                        String targetRaw = parsingParameters.get(i).getRaw();
                        boolean rawIsNull = targetRaw.equals("null") || String.valueOf(targetRaw).equals("null");
                        boolean baseRawIsNull = targetBaseRaw.equals("null") || String.valueOf(targetBaseRaw).equals("null");

                        // Find the parameter values for the target file
                        try {
                            // Check base-raw
                            if (targetBaseRaw.length() % 2 == 0) {// Четное число, можно преобразовывать к массиву байт
                                for (int j = 0; j < targetBaseRaw.length() / 2; j++)
                                    value = Byte.decode("0x" + targetBaseRaw.substring(j * 2, j * 2 + 2));
                            } else throw new NumberFormatException();
                            // Save float value
                        } catch (NullPointerException e) {
                        } catch (NumberFormatException e) {
                            // Search this parameter in parsingParametersArrayList
                            for (int j = 0; j < parsingParameters.size(); j++)
                                if (targetBaseRaw.equals(String.valueOf(parsingParameters.get(j).getId()))) {
                                    parsingParameters.get(i).setBase_raw(parsingParameters.get(j).getRaw());
                                }
                        }
//--------------------------------------------------------------------------------------------------------------------//


                        // Change the outputRequest parameter value depends on raw and baseRaw values
                        if (baseRawIsNull && !rawIsNull) {
                            parsingParameters.get(i).setOutputRequest(targetRaw);
                        }
                        if (!baseRawIsNull && rawIsNull) {
                            parsingParameters.get(i).setOutputRequest(parsingParameters.get(i).getBase_raw());
                        }

//------------------------The output in the log the list of parameters with dWord value-------------------------------//
//                        if (parsingParameters.get(i).getdWordNumber() != -1) {
//                            globalValues.add(fileTitle + " " + parsingParameters.get(i).toString());
//                            globalValues.add(fileTitle + "      " + parsingParameters.get(i).getStringLine());
//                        }
//--------------------------------------------------------------------------------------------------------------------//

//--------------------The output in the log the list of parameters with anotherValue variable-------------------------//
//                        if (parsingParameters.get(i).getAnotherValue() != null && !parsingParameters.get(i).getAnotherValue().equals("")) {
//                            globalValues.add(fileTitle + " " + parsingParameters.get(i).toString()); // All existing another values
//                            globalValues.add(parsingParameters.get(i).toStringForValues()); // All existing another values
//                        }
//--------------------------------------------------------------------------------------------------------------------//

//-------------------------The output in the log the list of parameters with bit variable-----------------------------//
//                        if (parsingParameters.get(i).getBitValue() != -1 && parsingParameters.get(i).getBitValue()>7) {
//                            globalValues.add(fileTitle + " " + parsingParameters.get(i).toString()); // All existing bit values
//                            globalValues.add(fileTitle + " " + parsingParameters.get(i).toStringForBits()); // All existing bit values
//                        }
//--------------------------------------------------------------------------------------------------------------------//

//----------------------The output in the log the list of all parameters existed parameters---------------------------//
                            globalValues.add("" + parsingParameters.get(i).toStringJustForId()); // All existing parameter names
//--------------------------------------------------------------------------------------------------------------------//

//----------------------------The output in the *.java file the list of parameters------------------------------------//
                        // Only decrypted parameters can be written to the *.java file (not with valueа, valueb,
                        // valueab and with unitValue == null)



                        if (parsingParameters.get(i).isDecryptedParameter()) {
                            // To verify compliance with the parameter list
///////////////////
//                            if (isEssentialParameter(parsingParameters.get(i).getId())) {
/////////
                                writer.println("        " + parsingParameters.get(i).getStringLine());

//---------------------------The output in the log the list of existing parameters------------------------------------//
//                                globalValues.add(parsingParameters.get(i).toString());
//--------------------------------------------------------------------------------------------------------------------//

//-----------------------The output in the log the list of parameters with zero value---------------------------------//
//                                if (parsingParameters.get(i).getValue() == 0)
//                                    globalValues.add(fileTitle + " " + parsingParameters.get(i).toString());
//--------------------------------------------------------------------------------------------------------------------//

/////////
//                            }
///////////////////
//                            writer.println("        " + parsingParameters.get(i).toString());
                        }
//--------------------------------------------------------------------------------------------------------------------//
//------The output in the log file the list of parameters with values raw.equals("null")&& base_raw.equals("null")----//
//                    if (parsingParameters.get(i).getOutputRequest().equals("none"))
//                            globalValues.add(String.valueOf(String.valueOf("INCLUDES=" + includesHashMap.size()) + " FileName=" +
//                                    fileTitle + " RAW=" + parsingParameters.get(i).getRaw() +
//                                    " BASE_RAW=" + parsingParameters.get(i).getBase_raw()) + " OUTPUT_REQUEST=" + parsingParameters.get(i).getOutputRequest());
//--------------------------------------------------------------------------------------------------------------------//
                    }
                    writer.println("        " + fileTitle + ".super.setName(\"" + fileTitle + "\");");
                    if (init_stringValues.size() != 0) {
                        String init_stringContent = "        " + fileTitle + ".super.setInit_string(new String[] {";
                        for (int j=0;j<init_stringValues.size();j++) {
                            if (j!=0) init_stringContent += ", ";
                            init_stringContent += "\"" + init_stringValues.get(j) + "\"";
                        }
                        init_stringContent += "});";
                        writer.println(init_stringContent);
                    }
                    writer.println("        " + fileTitle + ".super.setParameters(parameters);");
                    writer.println("    }");
                    writer.println("}");
                    writer.close();

//-----------------------Save the target file name in case of it is not default java word-----------------------------//
                    fileNames.add(fileTitle);
//--------------------------------------------------------------------------------------------------------------------//
                }
            } catch (FileNotFoundException e1) {
                e1.printStackTrace();
            } catch (UnsupportedEncodingException e1) {
                e1.printStackTrace();
            }
        }
    }

    private static Document parseXmlFile(String fileName){
        //get the factory
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        Document dom = null;
        try {
            //Using factory get an instance of document builder
            DocumentBuilder db = dbf.newDocumentBuilder();
            //parse using builder to get DOM representation of the XML file
            dom = (Document)db.parse(folder+"/"+fileName);
        }catch(ParserConfigurationException pce) {
            pce.printStackTrace();
        }catch(SAXException se) {
            se.printStackTrace();
        }catch(IOException ioe) {
            ioe.printStackTrace();
        }
        return dom;
    }

    private static String getValueWithoutSpace(String value) {
        if (value != null) {
            String newValue = "";
            for (int j = 0; j < value.length(); j++)
                if (!value.substring(j, j + 1).equals(" "))
                    newValue += value.substring(j, j + 1);
            return newValue;
        }
        return value;
    }

    private static ParsingParameter getParameter(Element paramEl, HashMap<String, ArrayList<ParsingParameter>> includesHashMap) {

        //For each <parameter> element get text or int values of
        //id ,unit, base_row and offset
        double[] valueAttributes = new double[2];
        double[] wordAttributes = new double[2];
        double[] wordLEAttributes = new double[2];
        double[] dWordAttributes = new double[2];
        double[] anotherValueAttributes = new double[3];
        double bitValue;
        for (int i=0;i<2;i++) {
            valueAttributes[i] = -1;
            wordAttributes[i] = -1;
            wordLEAttributes[i] = -1;
            dWordAttributes[i] = -1;
        }
        String anotherValue = "";
        String unit = getTextValue(paramEl, "unit");
//------------------------It's worked then the unit of the target parameter is absent---------------------------------//
        if (unit == null)
            unit = "";
//--------------------------------------------------------------------------------------------------------------------//

        // Raw and base-raw conversion (cutting the spaces)
        String raw = getValueWithoutSpace(String.valueOf(getTextValue(paramEl, "raw")));
        String base_raw = getValueWithoutSpace(String.valueOf(getTextValue(paramEl, "base-raw")));

        try {
            // Check the base-raw
            stringValue = base_raw;
            if (stringValue.length() % 2 == 0) {// It is an even number (it can be converted to an array of bytes)
                for (int j = 0; j < stringValue.length()/2; j++)
                    value = Byte.decode("0x" + stringValue.substring(j * 2, j * 2 + 2));
            } else throw new NumberFormatException();
        } catch (NullPointerException e) {
        } catch (NumberFormatException e) {
            if (!stringValue.equals("null")) {
                if (includesHashMap != null)
                    if (includesHashMap.size() > 0) {
                        for (String key : includesHashMap.keySet()) {
                            for (int i = 0; i < includesHashMap.get(key).size(); i++) {
                                if (includesHashMap.get(key).get(i).getId().equals(base_raw)) {
                                    base_raw = includesHashMap.get(key).get(i).getRaw();
                                    // Base-raw conversion (cutting the spaces)
                                    base_raw = getValueWithoutSpace(base_raw);
                                }
                            }
                        }
                    }
            }
        }

        float offset = getFloatValue(paramEl, "offset");

        valueAttributes = getDoubleSpecialValues(paramEl, "value", "-0");           // Случай, когда value-0XX

        if (valueAttributes[0] == -1)
            valueAttributes = getDoubleSpecialValues(paramEl, "value", "-");        // Случай, когда value-XX

        if (valueAttributes[0] == -1)
            valueAttributes = getDoubleSpecialValues(paramEl, "value-", "-");       // Случай, когда value--XX

        if (valueAttributes[0] == -1) {
            wordAttributes = getDoubleSpecialValues(paramEl, "word", "-");          // Случай, когда word-XX
            if (wordAttributes[0] == -1) {
                wordLEAttributes = getDoubleSpecialValues(paramEl, "wordle", "-");  // Случай, когда wordle-XX
                if (wordLEAttributes[0] == -1) {
                    dWordAttributes = getDoubleSpecialValues(paramEl, "dword", "-");// Случай, когда dword-XX
                    if (dWordAttributes[0] == -1)
                        anotherValue = getAnotherValue(paramEl);

                    if (anotherValue != null && !anotherValue.equals("")) {//AnotherValue is existed
                        anotherValueAttributes = getDoubleForAnotherValue(paramEl);
                    }

                    if (anotherValueAttributes != null) {
                        switch ((int)anotherValueAttributes[0]) {
                            case 0:// value
                                for (int i=0;i<valueAttributes.length;i++)
                                    valueAttributes[i] = anotherValueAttributes[i+1];
                                break;
                            case 1:// word
                                for (int i=0;i<wordAttributes.length;i++)
                                    wordAttributes[i] = anotherValueAttributes[i+1];
                                break;
                        }
                    }
                }
            }
        }

        bitValue = getDoubleValue(paramEl, "bit");

        String id = paramEl.getAttribute("id");
        ParsingParameter p = new ParsingParameter(id, (int)(valueAttributes[0]), valueAttributes[1],
        (int)(wordAttributes[0]), wordAttributes[1], (int)(wordLEAttributes[0]), wordLEAttributes[1],
                (int)dWordAttributes[0], dWordAttributes[1], (int)bitValue,
                anotherValue, unit, raw, base_raw, offset);
        return p;
    }
    private static double[] getDoubleSpecialValues(Element ele, String tagName, String separator) {
        double[] values = new double[2];
        for (int i=0;i<values.length;i++)
            values[i] = -1;
        NodeList nl;
        for (int i=0;i<150;i++) {
            nl = ele.getElementsByTagName(tagName + separator + i);
            if(nl != null && nl.getLength() > 0) {
                Element el = (Element)nl.item(0);
                String textVal = null;
                try {
                    textVal = el.getFirstChild().getNodeValue();
                } catch (NullPointerException e) {
                }
                try {
                    values[0] = i;
                    values[1] = Double.parseDouble(textVal);
                } catch (NullPointerException e) {
                }
                break;
            }
        }
        return values;
    }

    private static double getDoubleValue(Element ele, String tagName) {
        double value = -1;
        NodeList nl;
            nl = ele.getElementsByTagName(tagName);
            if(nl != null && nl.getLength() > 0) {
                Element el = (Element)nl.item(0);
                String textVal = null;
                try {
                    textVal = el.getFirstChild().getNodeValue();
                } catch (NullPointerException e) {
                }
                try {
                    value = Double.parseDouble(textVal);
                } catch (NullPointerException e) {
                }
            }
        return value;
    }

    private static final String[] valuesArray = {"a", "b", "c", "d", "e"};

    private static String getAnotherValue(Element ele) {
        String anotherValue = "";
        String currentValue;
        NodeList nl;
        for (int i=0;i<valuesArray.length;i++) {
            for (int j=0;j<valuesArray.length+1;j++) {
                if (j== valuesArray.length)
                    currentValue = "value" + valuesArray[i];
                else
                    currentValue = "value" + valuesArray[i] + valuesArray[j];
                nl = ele.getElementsByTagName(currentValue);
                if (nl != null && nl.getLength() > 0) {
                    anotherValue = currentValue;
                    break;
                }
            }
        }
        return anotherValue;
    }

    private static double[] getDoubleForAnotherValue(Element ele) {
        double[] values = null;
        String currentValue;
        String answer = "";
        NodeList nl;
        int firstValue = -1;
        int secondValue = -1;
        for (int i=0;i<valuesArray.length;i++) {
            for (int j=0;j<valuesArray.length+1;j++) {
                if (j== valuesArray.length)
                    currentValue = "value" + valuesArray[i];
                else
                    currentValue = "value" + valuesArray[i] + valuesArray[j];
                nl = ele.getElementsByTagName(currentValue);
                if (nl != null && nl.getLength() > 0) {
                    answer = currentValue;
                    firstValue = i;
                    secondValue = -1;
                    if (j != valuesArray.length)
                        secondValue = j;
                    break;
                }
            }
        }
        if (firstValue != -1) {// The target another value is existed
            values = new double[3];
            double attribute = getDoubleValue(ele, answer);
            if (secondValue == -1) {// value
                values[0] = 0;
                values[1] = firstValue + 3;// a - 3, b - 4, c - 5
                values[2] = attribute;
            } else if (firstValue<secondValue) {// word
                    values[0] = 1;
                    values[1] = firstValue + 3;
                    values[2] = attribute;
                } else values[0] = -1;
        }
        return values;
    }
    private static String getTextValue(Element ele, String tagName) {
        String textVal = null;
        NodeList nl = ele.getElementsByTagName(tagName);
        if(nl != null && nl.getLength() > 0) {
            Element el = (Element)nl.item(0);
            try {
                textVal = el.getFirstChild().getNodeValue();
            } catch (NullPointerException e) {
            }
        }
        return textVal;
    }
    private static float getFloatValue(Element ele, String tagName) {
        //in production application you would catch the exception
        float value = 0f;
        try {
            value = Float.parseFloat(getTextValue(ele, tagName));
        } catch (NullPointerException e) {
        }
        return value;
    }

    private static ArrayList<ParsingParameter> parseDocumentAndSearchParameters(Document dom, HashMap<String, ArrayList<ParsingParameter>> includesHashMap){
        ArrayList<ParsingParameter> parsingParameters = new ArrayList<ParsingParameter>();
        //get the root element
        Element docEle = dom.getDocumentElement();
        //get a nodelist of elements
        NodeList nl = docEle.getElementsByTagName("parameter");
        if(nl != null && nl.getLength() > 0) {
            for(int i = 0 ; i < nl.getLength();i++) {
                //get the parameter element
                Element el = (Element)nl.item(i);
                //get the Employee object
                ParsingParameter e = getParameter(el, includesHashMap);
                //add it to list
                parsingParameters.add(e);
            }
        }
        return parsingParameters;
    }

    private static ArrayList<String> parseDocumentAndSearchSectionByName(Document dom, String sectionName) {
        ArrayList<String> parsingArray = new ArrayList<String>();
        //get the root element
        Element docEle = dom.getDocumentElement();
        //get a nodelist of elements
        NodeList nl = docEle.getElementsByTagName(sectionName);
        if(nl != null && nl.getLength() > 0) {
            for(int i = 0 ; i < nl.getLength();i++) {
                //get the element value
                Element el = (Element)nl.item(i);
                try {
                    parsingArray.add(el.getFirstChild().getNodeValue());
                } catch (NullPointerException e) {
                }
            }
        }
        return parsingArray;
    }

    private static ArrayList<String> getInit_stringValues(String init_string) {
        ArrayList<String> init_stringValues = new ArrayList<String>();
        if (!init_string.equals("")) {
            String buffer = "";
            String value = "";
            for (int i = 0; i < init_string.length(); i++) {
                buffer = init_string.substring(i, i + 1);
                if (!buffer.equals(" ") && !buffer.equals("\n")) {// Cut all spaces and end of lines in the target string
                    if (buffer.equals(";")) {
                        init_stringValues.add(value);
                        value = "";
                    } else {
                        value += buffer;
                    }
                }
            }
        }
        return init_stringValues;
    }

    private static final String[] parametersList = {"CoolantTemp", "Speed",
            "BatteryVoltage",   /* and */   "ControlModuleVoltage", // Analogs
            "EngFuelRate",      /* and */   "LitersPerHour",        // Analogs
            "RPM", "MAF", "IntakeAirTemp", "STFT1", "STFT2", "LTFT1", "LTFT2",
            "ECUFuelLevel", "EngineLoad", "ThrottlePosition", "TimingAdvance",
            "MAP", "DistanceWithMIL", "DistanceWithoutMIL", "CommandedEGR",
            "EGRError", "EnginePercTorqueData", "CommEquivRatio"};

    // To verify compliance with the parameter list
    private static boolean isEssentialParameter(String parameterName) {
        for (String aParametersList : parametersList)
            if (parameterName.equals(aParametersList))
                return true;
        return false;
    }

    private static final String[] keywords = { "abstract", "assert", "boolean",
            "break", "byte", "case", "catch", "char", "class", "const",
            "continue", "default", "do", "double", "else", "extends", "false",
            "final", "finally", "float", "for", "goto", "if", "implements",
            "import", "instanceof", "int", "interface", "long", "native",
            "new", "null", "package", "private", "protected", "public",
            "return", "short", "static", "strictfp", "super", "switch",
            "synchronized", "this", "throw", "throws", "transient", "true",
            "try", "void", "volatile", "while" };

    private static boolean isJavaKeyword(String keyword) {
        return (Arrays.binarySearch(keywords, keyword) >= 0);
    }

    private static String checkFileNameForWrongLetters(String fileName) {
        String buffer = "";
        for (int i=0;i<fileName.length();i++) {
            if (fileName.substring(i,i+1).equals("-"))
                buffer += "_";
            else
                buffer += fileName.substring(i,i+1);
        }
        return buffer;
    }

    private static String folderDatabaseWasCreated = "";
    private static int numberOfXMLFiles = 0;
    private static File newFolder = null;
    private static String filePath = "";
    private static String folder = "";
    private static String messageListOfFiles = "";
    private static File[] files;

    private static String globalUnit;

    private static void saveLogs() {
        try {
            PrintWriter writer = new PrintWriter(newFolder+"/log.txt", "UTF-8");
                    /*1*/   if (!filePath.equals(""))                   writer.println("File path: "+filePath);
                    /*2*/   if (!folder.equals(""))                     writer.println("Target directory: "+folder);
                    /*3*/   if (!folderDatabaseWasCreated.equals(""))   writer.println(folderDatabaseWasCreated);
            else writer.println("Folder \"database\" exists");
                    /*4*/   if (!newFolder.equals(""))                  writer.println("Output directory: "+newFolder);
                    /*5*/   if (!messageListOfFiles.equals("")) {
                    writer.println(messageListOfFiles);
                    /*6*/   writer.println("Number of xml files = "+ numberOfXMLFiles);
            }
            else writer.println("No xml files in target directory were found");

            ArrayList<String> differentValues = new ArrayList<String>();

            // Служит для поиска параметров среди файлов
            if (globalValues.size()>0) {
                differentValues.add(globalValues.get(0));
                for (int i = 0; i < globalValues.size(); i++) {
                    boolean canAdd = true;
                    for (int j=0;j<differentValues.size();j++) {
                        try {
                            // Сохраним отличающиеся величины в отдельный список
                            if (differentValues.get(j).equals(globalValues.get(i)))
                                canAdd = false;
                        } catch (NullPointerException e) {
                        }
                    }
                    if (canAdd)
                        differentValues.add(globalValues.get(i));
                }

//-------------------------------------------Отсортируем строки по возростанию----------------------------------------//
                String targetValue;
                int targetNumber;
                ArrayList<String> sortedDifferentValues = new ArrayList<String>();
                    while (differentValues.size()>0) {
                        // Найдём первый элемент
                        targetNumber = 0;
                        targetValue = differentValues.get(0);
                        // Найдём индекс самой первой строки списка (по алфавиту)
                        for (int i=0;i<differentValues.size();i++)
                            if (targetValue.toLowerCase().compareTo(differentValues.get(i).toLowerCase()) > 0) {
                                targetValue = differentValues.get(i);
                                targetNumber = i;
                            }
                        // Вырежем из массива differentValues самую первую строку
                        sortedDifferentValues.add(differentValues.get(targetNumber));
                        differentValues.remove(targetNumber);
                    }
                // Перепишем строки обратно в список differentValues для сохранения структуры последующего кода
                for (String sortedDifferentValue : sortedDifferentValues) differentValues.add(sortedDifferentValue);
//--------------------------------------------------------------------------------------------------------------------//

                globalUnit = "";
                for (int i = 0; i < differentValues.size(); i++)
                    globalUnit += String.valueOf(i + 1) + " " + differentValues.get(i) + "\n";
                writer.println(globalUnit);
            }
            writer.close();
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        } catch (UnsupportedEncodingException e1) {
            e1.printStackTrace();
        }
    }
}


