package tirex.notebook.XMLFileParser;

/**
 * Created by antonobolenskov on 20/08/15.
 */
public class UnitValue {
    public static String[] getValue(String unit) {
        String ruValue = "";
        String enValue = "";
        if (unit.equals("bit"      )) {ruValue = "бит";         enValue = "bit";}

        else if (unit.equals("шагов"    )) {ruValue = "шаг";    enValue = "step";}
        else if (unit.equals("шаги"     )) {ruValue = "шаг";    enValue = "step";}
        else if (unit.equals("шаг"      )) {ruValue = "шаг";    enValue = "step";}
        else if (unit.equals("Шаг"      )) {ruValue = "шаг";    enValue = "step";}
        else if (unit.equals("Шаг"      )) {ruValue = "шаг";    enValue = "step";}

        else if (unit.equals("Volt"     )) {ruValue = "В";      enValue = "V";}
        else if (unit.equals("volts"    )) {ruValue = "В";      enValue = "V";}
        else if (unit.equals("V"        )) {ruValue = "В";      enValue = "V";}
        else if (unit.equals("mV"       )) {ruValue = "mВ";     enValue = "mV";}
        else if (unit.equals("В"        )) {ruValue = "В";      enValue = "V";}

        else if (unit.equals("°C"       )) {ruValue = "°С";     enValue = "°C";}
        else if (unit.equals("°С"       )) {ruValue = "°С";     enValue = "°C";}
        else if (unit.equals("celsius"  )) {ruValue = "°С";     enValue = "°C";}

        else if (unit.equals("kPa"      )) {ruValue = "кПа";    enValue = "kPa";}
        else if (unit.equals("MPa"      )) {ruValue = "МПа";    enValue = "MPa";}
        else if (unit.equals("кПа"      )) {ruValue = "кПа";    enValue = "kPa";}
        else if (unit.equals("МПа"      )) {ruValue = "МПа";    enValue = "MPa";}
        else if (unit.equals("Па"       )) {ruValue = "Па";     enValue = "Pa";}

        else if (unit.equals("мА"       )) {ruValue = "мА";     enValue = "mA";}
        else if (unit.equals("mA"       )) {ruValue = "мА";     enValue = "mA";}
        else if (unit.equals("A"        )) {ruValue = "А";      enValue = "A";}
        else if (unit.equals("А"        )) {ruValue = "А";      enValue = "A";}


        else if (unit.equals("hours"    )) {ruValue = "час";    enValue = "hour";}
        else if (unit.equals("minute"   )) {ruValue = "минута"; enValue = "minute";}
        else if (unit.equals("min"      )) {ruValue = "минута"; enValue = "minute";}
        else if (unit.equals("seconds"  )) {ruValue = "секунда";enValue = "second";}
        else if (unit.equals("sec"      )) {ruValue = "секунда";enValue = "second";}
        else if (unit.equals("Sec"      )) {ruValue = "секунда";enValue = "second";}
        else if (unit.equals("msec"     )) {ruValue = "мс";     enValue = "msec";}
        else if (unit.equals("мкс"      )) {ruValue = "мкс";    enValue = "us";}
        else if (unit.equals("мс"       )) {ruValue = "мс";     enValue = "mc";}

        else if (unit.equals("км"       )) {ruValue = "км";     enValue = "km";}
        else if (unit.equals("km"       )) {ruValue = "км";     enValue = "km";}

        else if (unit.equals("kg"       )) {ruValue = "кг";     enValue = "kg";}
        else if (unit.equals("g"        )) {ruValue = "г";      enValue = "g";}

        else if (unit.equals("ml"       )) {ruValue = "мл";     enValue = "ml";}
        else if (unit.equals("liter"    )) {ruValue = "литр";   enValue = "liter";}
        else if (unit.equals("liters"   )) {ruValue = "литр";   enValue = "liter";}

        else if (unit.equals("Watt"     )) {ruValue = "Ватт";   enValue = "Watt";}

        else if (unit.equals("km/h"     )) {ruValue = "км/час"; enValue = "km/h";}
        else if (unit.equals("км/час"   )) {ruValue = "км/час"; enValue = "km/h";}
        else if (unit.equals("kph"      )) {ruValue = "км/час"; enValue = "km/h";}

        else if (unit.equals("kgperhour")) {ruValue = "кг/час"; enValue = "kg/h";}

        else if (unit.equals("lphour"   )) {ruValue = "л/ч";    enValue = "l/h";}
        else if (unit.equals("lph"      )) {ruValue = "л/ч";    enValue = "l/h";}
        else if (unit.equals("мл/час"   )) {ruValue = "мл/ч";   enValue = "ml/h";}

        else if (unit.equals("Hz"       )) {ruValue = "Гц";     enValue = "Hz";}
        else if (unit.equals("Ohm"      )) {ruValue = "Ом";     enValue = "Ohm";}
        else if (unit.equals("kohms"    )) {ruValue = "кОм";    enValue = "kOhm";}

        else if (unit.equals("мм3"      )) {ruValue = "мм3";    enValue = "mm3";}

        else if (unit.equals("OFF/ON"   )) {ruValue = "Выкл/Вкл";enValue = "Off/On";}

        else if (unit.equals("бит. маска")){ruValue = "бит. маска";enValue = "bit mask";}

        else if (unit.equals("%"        )) {ruValue = "%";      enValue = "%";}
        else if (unit.equals("percent"  )) {ruValue = "%";      enValue = "%";}
        else if (unit.equals("шт"       )) {ruValue = "шт.";    enValue = "pieces";}

//        else if (unit.equals("rpm"      )) {ruValue = "null"; enValue = "null";}
//        else if (unit.equals("m/ss"     )) {ruValue = "null"; enValue = "null";}
//        else if (unit.equals("°"        )) {ruValue = "null"; enValue = "null";}
//        else if (unit.equals("ms"       )) {ruValue = "null"; enValue = "null";}
//        else if (unit.equals("v"        )) {ruValue = "null"; enValue = "null";}
//        else if (unit.equals("mmpc"     )) {ruValue = "null"; enValue = "null";}
//        else if (unit.equals("kpa"      )) {ruValue = "null"; enValue = "null";}
//        else if (unit.equals("degree"   )) {ruValue = "null"; enValue = "null";}
//        else if (unit.equals("bar"      )) {ruValue = "null"; enValue = "null";}
//        else if (unit.equals("grpersec" )) {ruValue = "null"; enValue = "null";}
//        else if (unit.equals("fahrenheit")) {ruValue = "null"; enValue = "null";}
//        else if (unit.equals("kw"       )) {ruValue = "null"; enValue = "null";}
//        else if (unit.equals("Nm"       )) {ruValue = "null"; enValue = "null";}
//        else if (unit.equals("inhg"     )) {ruValue = "null"; enValue = "null";}
//        else if (unit.equals("psi"      )) {ruValue = "null"; enValue = "null";}
//        else if (unit.equals("Millibar" )) {ruValue = "null"; enValue = "null";}
//        else if (unit.equals("degrees"  )) {ruValue = "null"; enValue = "null";}
//        else if (unit.equals("torr"     )) {ruValue = "null"; enValue = "null";}
//        else if (unit.equals("Om"       )) {ruValue = "null"; enValue = "null";}
//        else if (unit.equals("Bar"      )) {ruValue = "null"; enValue = "null";}
//        else if (unit.equals("mgpc"     )) {ruValue = "null"; enValue = "null";}
//        else if (unit.equals("kW"       )) {ruValue = "null"; enValue = "null";}
//        else if (unit.equals("RPM"      )) {ruValue = "null"; enValue = "null";}
//        else if (unit.equals("Num"      )) {ruValue = "null"; enValue = "null";}
//        else if (unit.equals("Amp"      )) {ruValue = "null"; enValue = "null";}
//        else if (unit.equals("Degree"   )) {ruValue = "null"; enValue = "null";}
//        else if (unit.equals("W"        )) {ruValue = "null"; enValue = "null";}
//        else if (unit.equals("hPa"      )) {ruValue = "null"; enValue = "null";}
//        else if (unit.equals("mg"       )) {ruValue = "null"; enValue = "null";}
//        else if (unit.equals("-"        )) {ruValue = "null"; enValue = "null";}
//        else if (unit.equals("кПа (Абс)")) {ruValue = "null"; enValue = "null";}
//        else if (unit.equals("кПа (gauge)")) {ruValue = "null"; enValue = "null";}
//        else if (unit.equals("kwt"      )) {ruValue = "null"; enValue = "null";}
//        else if (unit.equals("CA"       )) {ruValue = "null"; enValue = "null";}
//        else if (unit.equals("HEX"      )) {ruValue = "null"; enValue = "null";}
//        else if (unit.equals("об/мин"   )) {ruValue = "null"; enValue = "null";}
//        else if (unit.equals("g/rev"    )) {ruValue = "null"; enValue = "null";}
//        else if (unit.equals("л/100км"  )) {ruValue = "null"; enValue = "null";}
//        else if (unit.equals("№"        )) {ruValue = "null"; enValue = "null";}
//        else if (unit.equals("times"    )) {ruValue = "null"; enValue = "null";}
//        else if (unit.equals("pieces"   )) {ruValue = "null"; enValue = "null";}
//        else if (unit.equals("мг/ц"     )) {ruValue = "null"; enValue = "null";}
//        else if (unit.equals("Нм"       )) {ruValue = "null"; enValue = "null";}
//        else if (unit.equals("rpm/s^2"  )) {ruValue = "null"; enValue = "null";}
//        else if (unit.equals("h:min"    )) {ruValue = "null"; enValue = "null";}
        else {ruValue = unit; enValue = unit;}
        String[] units = new String[2];
        units[0] = ruValue;
        units[1] = enValue;
        return units;
    }
}
