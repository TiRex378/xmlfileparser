package tirex.notebook.XMLFileParser;

/**
 * Created by antonobolenskov on 20/08/15.
 */
public class ParsingParameter {
    private String id = "";
    private String unit = "";
    private String raw = "";
    private String base_raw = "";
    private double value = 0;
    private int valueNumber = -1;
    private double word = 0;
    private int wordNumber = -1;
    private double wordLE = 0;
    private int wordLENumber = -1;
    private double dWord = 0;
    private int dWordNumber = -1;
    private String anotherValue;
    private float offset = 0f;
    private int bitValue = -1;

    private String outputUnits[];
    private String outputRequest;
    private String answerPosition;
    private String footer;

    public ParsingParameter(String id, int valueNumber, double value, int wordNumber, double word,
                            int wordLENumber, double wordLE, int dWordNumber, double dWord,
                            int bitValue, String anotherValue, String unit, String raw, String base_raw, float offset) {
        this.id = id;
        this.valueNumber = valueNumber;
        this.value = value;
        this.wordNumber = wordNumber;
        this.word = word;
        this.wordLENumber = wordLENumber;
        this.wordLE = wordLE;
        this.dWordNumber = dWordNumber;
        this.dWord = dWord;
        this.anotherValue = anotherValue;
        this.unit = unit;
        this.raw = raw;
        this.base_raw = base_raw;
        this.bitValue = bitValue;

        outputRequest = "none";
        this.offset = offset;
        outputUnits = UnitValue.getValue(String.valueOf(unit));

//---------------------------------------------Calculate output values------------------------------------------------//
        answerPosition = "";
        boolean isWordLE = false;
        double multiplier = 0;
        String lineEnding = ", ";
        if (valueNumber != -1) {
            answerPosition += valueNumber;
            multiplier = value;
        } else if (wordNumber != -1) {
            answerPosition += wordNumber + ", " + String.valueOf(wordNumber + 1);
            multiplier = word;
        } else if (wordLENumber != -1) {
            isWordLE = true;
            answerPosition += wordLENumber + ", " + String.valueOf(wordLENumber + 1);
            multiplier = wordLE;
        } else if (dWordNumber != -1 ) {
            for (int i=0;i<4;i++) {
                if (i == 3)
                    lineEnding = "";
                answerPosition += String.valueOf(dWordNumber + i) + lineEnding;
            }
            multiplier = dWord;
        }
        footer = "";
        if (/*multiplier != 0 && */multiplier != 1) // 1 - default value
            footer += ".multiplier(" + multiplier + ")";
        if (offset != 0f)
            footer += ".offset(" + offset + ")";
        if (isWordLE)
            footer += ".wordLE(true)";
        if (bitValue != -1)
            footer += ".bitValue(" + bitValue + ")";
//--------------------------------------------------------------------------------------------------------------------//
    }
    public void setOutputRequest(String outputRequest) {
        this.outputRequest = outputRequest;
    }
    public String getOutputRequest() {
        return outputRequest;
    }
    public void setBase_raw(String base_raw) {
        this.base_raw = base_raw;
    }

    public String getId() {
        return id;
    }
    public double getValue() {
        return value;
    }
    public String getUnit() {
        return unit;
    }
    public String getRaw() {
        return raw;
    }
    public String getBase_raw() {
        return base_raw;
    }
    public int getdWordNumber() {
        return dWordNumber;
    }
    public String getAnotherValue() {
        return anotherValue;
    }
    public int getBitValue() {
        return bitValue;
    }
    public boolean isDecryptedParameter() {
        return !answerPosition.equals("");
    }

    @Override
    public String toString() {
        return "ID = " + id + " V_N=" + valueNumber + " V=" + value +
                " W_N=" + wordNumber + " W=" + word + " WLE_N=" + wordLENumber + " WLE=" + wordLE +
                " DW_N=" + dWordNumber + " DW=" + dWord + " U_RU=" + outputUnits[0] + " U_EN=" + outputUnits[1] +
                " AV=" + anotherValue + " B=" + bitValue + " RAW=" + raw + " B_R=" + base_raw +
                " O_R=" + outputRequest + " O=" + offset;
    }
    public String toStringForValues() {
        return " AV=" + anotherValue;
    }

    public String toStringForBits() {
        String av = "\"\"";
        String buffer = anotherValue;
        if (buffer.equals(""))
            buffer = av;
        return "ID=" + id + " W=" + word + " WLE=" + wordLE + " DW=" + dWord + " AV=" + buffer + " B=" + bitValue;
    }

    public String toStringJustForId() {
        return "ID=" + id;
    }

    public String getStringLine() {
        return "parameters.add(new Parameter.Builder(\"" + id + "\", \"" + outputRequest + "\", new int[] {" +
                answerPosition + "}, \"" + outputUnits[0] + "\", \"" + outputUnits[1] + "\")" + footer + ".build());";

    }
}